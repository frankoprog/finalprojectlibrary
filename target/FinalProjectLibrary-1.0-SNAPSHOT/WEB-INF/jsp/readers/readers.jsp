<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Catalog</title>
</head>
<body>
<fmt:setLocale value="${sessionScope.sessionLocale}"/>
<fmt:setBundle basename="langResources.langResources" var="loc"/>
<jsp:include page="../../../menu.jsp"/>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2><fmt:message bundle = "${loc}" key ="readers.list"/></h2></caption>
        <tr>
            <th>ID</th>
            <th><fmt:message bundle = "${loc}" key ="cabinet.email"/></th>

        </tr>
        <c:forEach var="user" items="${requestScope.readers}">
            <tr>
                <td><c:out value="${user.id}"/></td>
                <td><c:out value="${user.email}"/></td>
                <td>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=readersOrders">
                        <button type="submit" value=${user.id} name="userId"><fmt:message bundle = "${loc}" key ="readers.viewBooks"/></button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>

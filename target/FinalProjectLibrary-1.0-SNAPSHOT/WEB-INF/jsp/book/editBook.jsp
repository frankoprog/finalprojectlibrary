<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>addBook</title>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
<div class=loginContainer>
    <div class=form>
        <form method="post" action="${pageContext.request.contextPath}/controller?command=editBook">
            ID:
            <input type="text" value="${requestScope.bookId}" name="bookId" readonly><br/>
            Name:
            <input type="text" value="${requestScope.bookName}" name="bookName"><br/>
            Author:
            <input type="text" value="${requestScope.authorName}" name="authorName"><br/>
            Publisher:
            <input type="text" value="${requestScope.publisherName}" name="publisherName"><br/>
            <input type="submit" value=Change>
        </form>
    </div>
</div>
</body>
</html>

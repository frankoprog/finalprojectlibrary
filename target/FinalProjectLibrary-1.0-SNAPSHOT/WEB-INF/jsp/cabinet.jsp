<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Cabinet</title>
    <link rel="stylesheet" href="../../css/table.css">
    <link rel="stylesheet" href="../../css/readerMainStyle.css">
    <link rel="stylesheet" href="../../css/bookCard.css">
</head>
<body>
<fmt:setLocale value="${sessionScope.sessionLocale}"/>
<fmt:setBundle basename="langResources.langResources" var="loc"/>
<jsp:include page="../../menu.jsp"/>
<div class="profileContainer">
    <div class="basicInfo">
        <div class="tab-pane active in" id="home">
            <form class="readerInfo">
                <label><fmt:message bundle="${loc}" key="cabinet.firstName"/> </label>
                <c:out value="${requestScope.name}"/>
                <br>
                <label><fmt:message bundle="${loc}" key="cabinet.lastName"/> </label>
                <c:out value="${requestScope.surname}"/>
                <br>
                <label><fmt:message bundle="${loc}" key="cabinet.email"/> </label>
                <c:out value="${requestScope.email}"/>
                <br>
                <label><fmt:message bundle="${loc}" key="cabinet.phone"/> </label>
                <c:out value="${requestScope.phone}"/>
            </form>
        </div>
    </div>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2><fmt:message bundle="${loc}" key="cabinet.orderList"/></h2></caption>
            <tr>
                <th><fmt:message bundle="${loc}" key="orders.orderId"/></th>
                <th><fmt:message bundle="${loc}" key="catalog.title"/></th>
                <th><fmt:message bundle="${loc}" key="cabinet.date"/></th>
                <th><fmt:message bundle="${loc}" key="cabinet.status"/></th>
<%--                <th>Action</th>--%>
            </tr>
            <td>
                <c:set scope="request" var="infoNumberFlag" value="0"/>
                <c:forEach var="order" items="${requestScope.myOrders}">
                    <c:forEach var="order_with" items="${requestScope.myOrdersWith}">
                        <c:forEach var="book" items="${requestScope.myBooks}">
                            <c:if test="${order.idOrder == order_with.id&&order.idBook==book.id}">
                                <p><c:out value="${order.idOrder} "/></p>
                                <c:set scope="request" var="infoNumberFlag" value="1"/>
                            </c:if>
                        </c:forEach>
                    </c:forEach>
                </c:forEach>
            </td>
            <td>
                <c:set scope="request" var="infoNumberFlag" value="0"/>
                <c:forEach var="order" items="${requestScope.myOrders}">
                    <c:forEach var="order_with" items="${requestScope.myOrdersWith}">
                        <c:forEach var="book" items="${requestScope.myBooks}">
                            <c:if test="${order.idOrder == order_with.id&&order.idBook==book.id}">
                                <p><c:out value="${book.name}"/></p>
                                <c:set scope="request" var="infoNumberFlag" value="1"/>
                            </c:if>
                        </c:forEach>
                    </c:forEach>
                </c:forEach>
            </td>
            <td>
                <c:set scope="request" var="infoNumberFlag" value="0"/>
                <c:forEach var="order" items="${requestScope.myOrders}">
                    <c:forEach var="order_with" items="${requestScope.myOrdersWith}">
                        <c:forEach var="book" items="${requestScope.myBooks}">
                            <c:if test="${order.idOrder == order_with.id&&order.idBook==book.id}">
                                <p><c:out value="${order_with.orderDue}"/></p>
                                <c:set scope="request" var="infoNumberFlag" value="1"/>
                            </c:if>
                        </c:forEach>
                    </c:forEach>
                </c:forEach>
            </td>
            <td>
                <c:set scope="request" var="infoNumberFlag" value="0"/>
                <c:forEach var="order" items="${requestScope.myOrders}">
                    <c:forEach var="order_with" items="${requestScope.myOrdersWith}">
                        <c:forEach var="book" items="${requestScope.myBooks}">
                            <c:if test="${order.idOrder == order_with.id&&order.idBook==book.id}">
                                <p><c:out value="${order_with.status}"/></p>
                                <c:set scope="request" var="infoNumberFlag" value="1"/>
                            </c:if>
                        </c:forEach>
                    </c:forEach>
                </c:forEach>
            </td>
<%--            <td>--%>
<%--                <c:forEach var="order_with" items="${requestScope.myOrdersWith}">--%>
<%--                    <form method="post" action="${pageContext.request.contextPath}/controller?command=payFine">--%>
<%--                        <button type="submit" value=${requestScope.order.idOrder} name="orderId"><fmt:message--%>
<%--                                bundle="${loc}" key="cabinet.payFine"/></button>--%>
<%--                    </form>--%>
<%--                </c:forEach>--%>
<%--            </td>--%>
        </table>
    </div>
</div>
</body>
</html>

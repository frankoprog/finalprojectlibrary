package com.epam.franko.finalprojectlibrary.service;

import com.epam.franko.finalprojectlibrary.dao.RoleDaoImpl;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.RoleDAO;
import com.epam.franko.finalprojectlibrary.interfaces.service.RoleService;
import com.epam.franko.finalprojectlibrary.model.Role;

import java.util.List;
import java.util.Optional;

public class RoleServiceImpl extends ServiceImpl implements RoleService {
    public RoleServiceImpl() throws ServiceException {
    }

    /**
     * Find list of all roles
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    @Override
    public List<Role> findAll() throws ServiceException {
        RoleDAO roleDao = new RoleDaoImpl();
        List<Role> roles = null;
        try {
            roles = roleDao.findAll();
        } catch (DaoException e) {
            LOG.info(e);
        }
        return roles;

    }

    /**
     * Find role by id.
     *
     * @param id the id
     * @return the role
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<Role> findById(Integer id) throws ServiceException {
        Optional<Role> role = null;
        RoleDAO roleDao = new RoleDaoImpl();
        try {
            role = roleDao.findById(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return role;
    }

    /**
     * Save role
     *
     * @param role the role
     * @return the integer
     * @throws ServiceException the service exception
     */
    @Override
    public Integer save(Role role) throws ServiceException {
        try {
            return new RoleDaoImpl().save(role);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return -1;
    }

    /**
     * Delete role by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    @Override
    public void delete(Integer id) throws ServiceException {
        RoleDAO roleDao = new RoleDaoImpl();
        try {
            roleDao.delete(id);
        } catch (DaoException e) {
            LOG.info(e);
        }

    }
}

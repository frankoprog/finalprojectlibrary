package com.epam.franko.finalprojectlibrary.service;


import com.epam.franko.finalprojectlibrary.dao.UserOrderDaoImpl;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.UserOrderDAO;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserOrderService;
import com.epam.franko.finalprojectlibrary.model.UserOrder;

import java.util.List;
import java.util.Optional;

public class UserOrderServiceImpl extends ServiceImpl implements UserOrderService {
    /**
     * Find list of all user_orders
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    @Override
    public List<UserOrder> findAll() throws ServiceException {
        List<UserOrder> userOrders = null;
        UserOrderDAO userOrderDAO = new UserOrderDaoImpl();
        try {
            userOrders = userOrderDAO.findAll();
        } catch (DaoException e) {
            LOG.info(e);
        }
        return userOrders;
    }

    /**
     * Find user_order by user_order_id.
     *
     * @param id the user_order_Id
     * @return finds user_user_order by user_orderId
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<UserOrder> findByOrderId(Integer id) throws ServiceException {
        Optional<UserOrder> userOrder = null;
        UserOrderDAO userOrderDao = new UserOrderDaoImpl();
        try {
            userOrder = userOrderDao.findByOrderId(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return userOrder;
    }

    /**
     * @param userId user's id
     * @return finds user_user_order by userId
     * @throws ServiceException
     */
    @Override
    public List<UserOrder> findByUserId(Integer userId) throws ServiceException {
        List<UserOrder> userOrders = null;
        UserOrderDAO userOrderDao = new UserOrderDaoImpl();
        try {
            userOrders = userOrderDao.findByUserId(userId);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return userOrders;
    }

    /**
     * @param bookId book's id
     * @return finds user_user_order by bookId
     * @throws ServiceException
     */
    @Override
    public List<UserOrder> findByBookId(Integer bookId) throws ServiceException {
        List<UserOrder> userOrders = null;
        UserOrderDAO userOrderDao = new UserOrderDaoImpl();
        try {
            userOrders = userOrderDao.findByBookId(bookId);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return userOrders;
    }

    /**
     * Save user_order
     *
     * @param userOrder the user_order
     * @return the integer
     * @throws ServiceException the service exception
     */
    @Override
    public Integer save(UserOrder userOrder) throws ServiceException {
        try {
            return new UserOrderDaoImpl().save(userOrder);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return -1;
    }

    /**
     * update Order
     *
     * @param userOrder
     * @throws ServiceException
     */
    @Override
    public void update(UserOrder userOrder) throws ServiceException {
        new UserOrderDaoImpl().update(userOrder);
    }

    /**
     * Delete user_order by orderId
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    @Override
    public void delete(Integer id) throws ServiceException {
        UserOrderDAO userOrderDao = new UserOrderDaoImpl();
        try {
            userOrderDao.delete(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
    }
}

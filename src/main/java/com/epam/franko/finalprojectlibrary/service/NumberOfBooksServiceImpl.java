package com.epam.franko.finalprojectlibrary.service;

import com.epam.franko.finalprojectlibrary.dao.NumberOfBooksDaoImpl;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.NumberOfBooksDAO;
import com.epam.franko.finalprojectlibrary.interfaces.service.NumberOfBooksService;
import com.epam.franko.finalprojectlibrary.model.NumberOfBooks;

import java.util.List;
import java.util.Optional;

public class NumberOfBooksServiceImpl extends ServiceImpl implements NumberOfBooksService {
    public NumberOfBooksServiceImpl() throws ServiceException {
    }

    /**
     * Find list of all numberOfBooks
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    @Override
    public List<NumberOfBooks> findAll() throws ServiceException {
        List<NumberOfBooks> numberOfBooks = null;
        NumberOfBooksDaoImpl numberOfBooksDao = new NumberOfBooksDaoImpl();
        try {
            numberOfBooks = numberOfBooksDao.findAll();
        } catch (DaoException e) {
            LOG.info(e);
        }
        return numberOfBooks;
    }

    /**
     * Find numberOfBook by book_id.
     *
     * @param id the book_id
     * @return numberOfBook
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<NumberOfBooks> findById(Integer id) throws ServiceException {
        Optional<NumberOfBooks> numberOfBooks = null;
        NumberOfBooksDaoImpl numberOfBooksDao = new NumberOfBooksDaoImpl();
        try {
            numberOfBooks = numberOfBooksDao.findByBookId(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return numberOfBooks;
    }

    /**
     * Save numberOfBooks
     *
     * @param numberOfBooks the numberOfBooks
     * @return the integer
     * @throws ServiceException the service exception
     */
    @Override
    public Integer save(NumberOfBooks numberOfBooks) throws ServiceException {
        try {
            return new NumberOfBooksDaoImpl().save(numberOfBooks);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return -1;
    }

    /**
     * Delete numberOfBooks by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    @Override
    public void delete(Integer id) throws ServiceException {
        NumberOfBooksDAO numberOfBooksDao = new NumberOfBooksDaoImpl();
        try {
            numberOfBooksDao.delete(id);
        } catch (DaoException e) {
            LOG.info(e);
        }

    }
}

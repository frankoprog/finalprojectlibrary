package com.epam.franko.finalprojectlibrary.service;

import com.epam.franko.finalprojectlibrary.dao.BookDaoImpl;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.BookDAO;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.model.Book;

import java.util.List;
import java.util.Optional;

public class BookServiceImpl extends ServiceImpl implements BookService {


    public BookServiceImpl() throws ServiceException {
        super();
    }

    /**
     * Find list of all books
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    @Override
    public List<Book> findAll() throws ServiceException {
        List<Book> books = null;
        BookDAO bookDao = new BookDaoImpl();
        try {
            books = bookDao.findAll();
        } catch (DaoException e) {
            LOG.info(e);
        }
        return books;
    }

    /**
     * Find book by id.
     *
     * @param id the id
     * @return the book
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<Book> findById(Integer id) throws ServiceException {
        Optional<Book> book = null;
        BookDAO bookDao = new BookDaoImpl();
        try {
            book = bookDao.findById(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return book;
    }

    /**
     * Check if book exist in database
     *
     * @param name the name
     * @return the boolean
     * @throws ServiceException the service exception
     */
    @Override
    public boolean isExist(String name) throws ServiceException {
        BookDAO bookDao = new BookDaoImpl();
        String tempName = null;
        try {
            tempName = String.valueOf(bookDao.findByName(name));
            return !(tempName == null);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return false;
    }

    /**
     * Find user by author.
     *
     * @param author the author
     * @return the book
     * @throws ServiceException the service exception
     */
    @Override
    public List<Book> findByAuthor(String author) throws ServiceException {
        List<Book> books = null;
        BookDAO bookDao = new BookDaoImpl();
        try {
            books = bookDao.findByAuthor(author);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return books;
    }

    /**
     * @param name
     * @return
     * @throws ServiceException
     */
    @Override
    public List<Book> findByName(String name) throws ServiceException {
        List<Book> books = null;
        BookDAO bookDao = new BookDaoImpl();
        try {
            books = bookDao.findByName(name);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return books;
    }

    /**
     * Save book
     *
     * @param book the book
     * @return the integer
     * @throws ServiceException the service exception
     */
    @Override
    public Integer save(Book book) throws ServiceException {
        try {
            return new BookDaoImpl().save(book);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return -1;
    }

    /**
     * @param book
     * @throws ServiceException
     */
    @Override
    public void update(Book book) throws ServiceException {
        new BookDaoImpl().update(book);
    }

    /**
     * Delete book by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    @Override
    public void delete(Integer id) throws ServiceException {
        BookDAO bookDao = new BookDaoImpl();
        try {
            bookDao.delete(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
    }
}

package com.epam.franko.finalprojectlibrary.service;

import com.epam.franko.finalprojectlibrary.dao.UserInfoDaoImpl;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.UserInfoDAO;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserInfoService;
import com.epam.franko.finalprojectlibrary.model.UserInfo;

import java.util.List;
import java.util.Optional;

public class UserInfoServiceImpl extends ServiceImpl implements UserInfoService {

    public UserInfoServiceImpl() throws ServiceException {
    }

    /**
     * Find list of all userInfos
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    @Override
    public List<UserInfo> findAll() throws ServiceException {
        List<UserInfo> userInfos = null;
        UserInfoDAO userInfoDao = new UserInfoDaoImpl();
        try {
            userInfos = userInfoDao.findAll();
        } catch (DaoException e) {
            LOG.info(e);
        }
        return userInfos;
    }

    /**
     * Find userInfo by user_id.
     *
     * @param id the user_id
     * @return numberOfBook
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<UserInfo> findById(Integer id) throws ServiceException {
        Optional<UserInfo> userInfo = null;
        UserInfoDAO userInfoDao = new UserInfoDaoImpl();
        try {
            userInfo = userInfoDao.findByUserId(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return userInfo;
    }

    /**
     * Save userInfo
     *
     * @param userInfo the userInfo
     * @return the integer
     * @throws ServiceException the service exception
     */
    @Override
    public Integer save(UserInfo userInfo) throws ServiceException {
        try {
            return new UserInfoDaoImpl().save(userInfo);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return -1;
    }

    /**
     * Delete userInfo by user_id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    @Override
    public void delete(Integer id) throws ServiceException {
        UserInfoDAO userInfoDao = new UserInfoDaoImpl();
        try {
            userInfoDao.delete(id);
        } catch (DaoException e) {
            LOG.info(e);
        }


    }
}

package com.epam.franko.finalprojectlibrary.service;

import com.epam.franko.finalprojectlibrary.dao.OrderDaoImpl;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.OrderDAO;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.model.Order;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class OrderServiceImpl extends ServiceImpl implements OrderService {
    public OrderServiceImpl() throws ServiceException {
    }

    /**
     * Find list of all orders
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    @Override
    public List<Order> findAll() throws ServiceException {
        List<Order> orders = null;
        OrderDAO orderDao = new OrderDaoImpl();
        try {
            orders = orderDao.findAll();
        } catch (DaoException e) {
            LOG.info(e);
        }
        return orders;
    }

    /**
     * Find order by user_id.
     *
     * @param id the user_id
     * @return numberOfBook
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<Order> findById(Integer id) throws ServiceException {
        Optional<Order> order = null;
        OrderDAO orderDao = new OrderDaoImpl();
        try {
            order = orderDao.findById(id);
        } catch (DaoException | SQLException e) {
            LOG.info(e);
        }
        return order;
    }

    /**
     * @param status
     * @return
     * @throws ServiceException
     */
    @Override
    public List<Order> findByStatus(String status) throws ServiceException {
        List<Order> orders = null;
        OrderDAO orderDao = new OrderDaoImpl();
        try {
            orders = orderDao.findByStatus(status);
        } catch (DaoException | SQLException e) {
            LOG.info(e);
        }
        return orders;
    }

    /**
     * Save order
     *
     * @param order the order
     * @return the integer
     * @throws ServiceException the service exception
     */
    @Override
    public Integer save(Order order) throws ServiceException {
        try {
            return new OrderDaoImpl().save(order);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return -1;
    }

    public void update(Order order) throws ServiceException {
        new OrderDaoImpl().update(order);
    }

    /**
     * Delete order by user_id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    @Override
    public void delete(Integer id) throws ServiceException {
        OrderDAO orderDao = new OrderDaoImpl();
        try {
            orderDao.delete(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
    }
}

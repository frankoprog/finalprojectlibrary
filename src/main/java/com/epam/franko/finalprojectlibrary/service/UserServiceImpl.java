package com.epam.franko.finalprojectlibrary.service;

import com.epam.franko.finalprojectlibrary.dao.RoleDaoImpl;
import com.epam.franko.finalprojectlibrary.dao.UserDaoImpl;
import com.epam.franko.finalprojectlibrary.dao.UserInfoDaoImpl;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.exceptions.ValidationException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.RoleDAO;
import com.epam.franko.finalprojectlibrary.interfaces.dao.UserDAO;
import com.epam.franko.finalprojectlibrary.interfaces.dao.UserInfoDAO;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserService;
import com.epam.franko.finalprojectlibrary.model.Role;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.model.UserInfo;
import com.epam.franko.finalprojectlibrary.validate.UserValidator;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl extends ServiceImpl implements UserService {
    private UserValidator userValidator;

    public UserServiceImpl() throws ServiceException {
        userValidator = new UserValidator();
    }

    /**
     * Find list of all users
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    @Override
    public List<User> findAll() throws ServiceException {
        List<User> users = null;
        UserDaoImpl userDao = new UserDaoImpl();
        try {
            users = userDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
        return users;
    }

    /**
     * Find user by id.
     *
     * @param id the id
     * @return the user
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<User> findById(Integer id) throws ServiceException {
        Optional<User> user = null;
        UserDaoImpl userDao = new UserDaoImpl();
        try {
            user = userDao.findById(id);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return user;
    }

    /**
     * Check if user exist in database
     *
     * @param email the email
     * @return the boolean
     * @throws ServiceException the service exception
     */
    @Override
    public boolean isExist(String email) throws ServiceException {
        UserDAO userDao = new UserDaoImpl();
        String tempEmail = null;
        try {
            tempEmail = String.valueOf(userDao.findByEmail(email));
            return !(tempEmail == null);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return false;
    }

    /**
     * Find user by email and password.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     * @throws ServiceException the service exception
     */
    @Override
    public Optional<User> findByEmailAndPassword(String email, String password) throws ServiceException {
        Optional<User> user = null;
        UserDAO userDao = new UserDaoImpl();
        try {
            user = userDao.findByEmailAndPassword(email, password);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return user;

    }

    /**
     * Find user by role
     *
     * @param role_id role id
     * @return
     * @throws ServiceException
     */
    @Override
    public List<User> findByRoleId(int role_id) throws ServiceException {
        List<User> users = null;
        UserDAO userDao = new UserDaoImpl();
        try {
            users = userDao.findByRole(role_id);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return users;
    }

    /**
     * Save user
     *
     * @param user the user
     * @return the integer
     * @throws ServiceException the service exception
     */
    @Override
    public Integer save(User user) throws ServiceException {
        try {
            userValidator.isValid(user);
        } catch (ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
        try {
            return new UserDaoImpl().save(user);
        } catch (DaoException e) {
            LOG.info(e);
        }
        return -1;
    }

    /**
     * Delete user by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    @Override
    public void delete(Integer id) throws ServiceException {
        UserDAO userDao = new UserDaoImpl();
        try {
            userDao.delete(id);
        } catch (DaoException e) {
            LOG.info(e);
        }

    }

    /**
     * Register new user in database
     *
     * @param name     new users name
     * @param surname  new users surname
     * @param email    new users email
     * @param phone    new users phone
     * @param password new users password
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean register(String name, String surname, String email, String phone, String password) throws ServiceException {
        if (email == null || password == null) {
            return false;
        }
        try {
            UserDAO userDao = new UserDaoImpl();
            if (userDao.findByEmail(email).isPresent()) {
                return false;
            }
            RoleDAO roleDao = new RoleDaoImpl();
            Optional<Role> role = roleDao.findById(1);
            if (!role.isPresent()) {
                return false;
            }
            User user = new User();
            user.setId(0);
            user.setEmail(email);
            user.setPassword(password);
            user.setRoleId(role.get().getId());
            int userid = userDao.save(user);
            UserInfoDAO userInfoDao = new UserInfoDaoImpl();
            UserInfo userInfo = new UserInfo();
            userInfo.setId(userid);
            userInfo.setName(name);
            userInfo.setSurname(surname);
            userInfo.setPhone(phone);
            userInfoDao.save(userInfo);
            return true;
        } catch (DaoException e) {
            LOG.info(e);
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private boolean isEmailValid(String email) {
        try {
            return userValidator.isValid(new User(0, email, "test", 0));
        } catch (ValidationException e) {
            LOG.info(e);
        }
        return false;
    }
}

package com.epam.franko.finalprojectlibrary.tag;

import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.sort.AuthorNameSortByAsc;
import com.epam.franko.finalprojectlibrary.sort.BookNameSortByAsc;

import java.util.Collections;
import java.util.List;

public final class BookSort {
    public static List<Book> sortByName(List<Book> books) {
        if (books == null) return null;
        Collections.sort(books, new BookNameSortByAsc());
        return books;
    }

    public static List<Book> sortByAuthor(List<Book> books) {
        if (books == null) return null;
        Collections.sort(books, new AuthorNameSortByAsc());
        return books;
    }
}

package com.epam.franko.finalprojectlibrary.model;

import java.util.Objects;

public class UserOrder {
    private int idOrder;
    private int idUser;
    private int idBook;

    public UserOrder(int idOrder, int idUser, int idBook) {
        setIdOrder(idOrder);
        setIdUser(idUser);
        setIdBook(idBook);
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserOrder userOrder = (UserOrder) o;
        return idOrder == userOrder.idOrder && idUser == userOrder.idUser && idBook == userOrder.idBook;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idOrder, idUser, idBook);
    }

    @Override
    public String toString() {
        return "UserOrder{" +
                "idOrder=" + idOrder +
                ", idUser=" + idUser +
                ", idBook=" + idBook +
                '}';
    }
}

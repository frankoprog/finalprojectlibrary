package com.epam.franko.finalprojectlibrary.model;

import java.util.Objects;

public class NumberOfBooks {
    private int idBook;
    private int number;

    public NumberOfBooks(int idBook, int number) {
        setIdBook(idBook);
        setNumber(number);
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumberOfBooks that = (NumberOfBooks) o;
        return idBook == that.idBook && number == that.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idBook, number);
    }

    @Override
    public String toString() {
        return "NumberOfBooks{" +
                "idBook=" + idBook +
                ", number=" + number +
                '}';
    }
}
package com.epam.franko.finalprojectlibrary.model;

import java.util.Objects;

public class Book {
    private int id;
    private String name;
    private String author;
    private String publisherName;
    private String synopsis;
    private String photo;


    public Book(int id, String name, String author,String publisherName, String synopsis, String photo) {
        setId(id);
        setPublisherName(publisherName);
        setName(name);
        setSynopsis(synopsis);
        setAuthor(author);
        setPhoto(photo);
    }

    public Book() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id && publisherName == book.publisherName && name.equals(book.name) &&
                author.equals(book.author) && synopsis.equals(book.synopsis) && photo.equals(book.photo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, publisherName, name,author, synopsis, photo);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", publisherName='" + publisherName + '\'' +
                ", synopsis='" + synopsis + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}

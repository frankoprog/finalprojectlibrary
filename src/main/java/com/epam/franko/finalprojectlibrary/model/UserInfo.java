package com.epam.franko.finalprojectlibrary.model;

import java.util.Objects;

public class UserInfo {
    private int id;
    private String name;
    private String surname;
    private String phone;

    public UserInfo() {

    }

    public UserInfo(int id, String name, String surname, String phone) {
        setId(id);
        setName(name);
        setSurname(surname);
        setPhone(phone);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return id == userInfo.id && phone == userInfo.phone && name.equals(userInfo.name) && surname.equals(userInfo.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, phone);
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phone=" + phone +
                '}';
    }
}

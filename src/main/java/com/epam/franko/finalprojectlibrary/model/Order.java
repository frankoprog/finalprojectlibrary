package com.epam.franko.finalprojectlibrary.model;

import java.util.Date;
import java.util.Objects;

public class Order {
    private int id;
    private Date orderDue;
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOrderDue() {
        return orderDue;
    }

    public void setOrderDue(Date orderDate) {
        this.orderDue = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Order(int id, Date orderDue, String status) {
        setId(id);
        setOrderDue(orderDue);
        setStatus(status);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && Objects.equals(orderDue, order.orderDue) && Objects.equals(status, order.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,orderDue, status);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderDue=" + orderDue +
                ", status='" + status + '\'' +
                '}';
    }
}


package com.epam.franko.finalprojectlibrary.model;

public class User {
    private int id;
    private String email;
    private String password;
    private int roleId;

    public User() {

    }

    public User(int id,String email, String password, int roleId) {
        setId(id);
        setEmail(email);
        setPassword(password);
        setRoleId(roleId);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        User user = (User) obj;
        return id == user.id &&
                email.equals(user.email) &&
                password.equals(user.password) &&
                roleId == user.roleId;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("User:{");
        sb.append("id = ").append(id)
                .append("email = ").append(email)
                .append(", password = ").append(password).append("\r\n")
                .append("roleId = ").append(roleId).append('}');
        return sb.toString();
    }
}

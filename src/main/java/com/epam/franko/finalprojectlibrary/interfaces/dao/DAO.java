package com.epam.franko.finalprojectlibrary.interfaces.dao;

import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.util.List;

public interface DAO<T> {
    /**
     * finds all items
     * @return list of all objects
     * @throws DaoException
     */
    List<T> findAll() throws DaoException;

    /**
     * save an object into Database
     * @param item an object that will be added to database
     * @return
     * @throws DaoException
     */
    int save(T item) throws DaoException;

    /**
     * deletes an item in database
     * @param id item's id that will be erased
     * @throws DaoException
     */
    void delete(int id) throws DaoException;

    /**
     * updates an item in database
     * @param item item that will be updated with it's new info
     */
    void update(T item);
}

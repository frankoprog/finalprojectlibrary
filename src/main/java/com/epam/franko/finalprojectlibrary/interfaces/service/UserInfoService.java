package com.epam.franko.finalprojectlibrary.interfaces.service;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.model.UserInfo;

import java.util.List;
import java.util.Optional;

public interface UserInfoService {

    /**
     * Find list of all userInfos
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<UserInfo> findAll() throws ServiceException;

    /**
     * Find userInfo by user_id.
     *
     * @param id the user_id
     * @return numberOfBook
     * @throws ServiceException the service exception
     */
    Optional<UserInfo> findById(Integer id) throws ServiceException;

    /**
     * Save userInfo
     *
     * @param userInfo the userInfo
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer save(UserInfo userInfo) throws ServiceException;

    /**
     * Delete userInfo by user_id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    void delete(Integer id) throws ServiceException;
}

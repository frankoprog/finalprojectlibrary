package com.epam.franko.finalprojectlibrary.interfaces.dao;

import com.epam.franko.finalprojectlibrary.model.NumberOfBooks;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.util.Optional;

public interface NumberOfBooksDAO extends DAO<NumberOfBooks> {
    Optional<NumberOfBooks> findByBookId(int id) throws DaoException;
}

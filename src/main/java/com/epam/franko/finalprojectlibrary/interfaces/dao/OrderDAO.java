package com.epam.franko.finalprojectlibrary.interfaces.dao;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface OrderDAO extends DAO<Order> {
    /**
     * Find order by user_id.
     *
     * @param id the user_id
     * @return numberOfBook
     * @throws DaoException,SQLException
     */
    Optional<Order> findById(int id) throws DaoException, SQLException;
    /**
     * @param status
     * @return
     * @throws ServiceException
     */
    List<Order> findByStatus(String status) throws DaoException, SQLException;
}

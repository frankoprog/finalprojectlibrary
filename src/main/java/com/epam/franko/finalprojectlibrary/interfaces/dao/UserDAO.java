package com.epam.franko.finalprojectlibrary.interfaces.dao;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.util.List;
import java.util.Optional;

public interface UserDAO extends DAO<User> {
    /**
     * Find user by id.
     *
     * @param id the id
     * @return the user
     * @throws DaoException
     */
    Optional<User> findById(int id) throws DaoException;
    /**
     * Find user by email.
     *
     * @param email user's email
     * @return the user
     * @throws DaoException
     */
    Optional<User> findByEmail(String email) throws DaoException;
    /**
     * Find user by email and password.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     * @throws DaoException
     */
    Optional<User> findByEmailAndPassword(String email, String password) throws DaoException;
    /**
     * Find user by role
     * @param role_id role id
     * @return
     * @throws DaoException
     */
    List<User> findByRole(int role_id) throws DaoException;

}

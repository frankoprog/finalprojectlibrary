package com.epam.franko.finalprojectlibrary.interfaces.dao;

import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.util.List;
import java.util.Optional;

public interface BookDAO extends DAO<Book> {
    /**
     * Finds a Book by its id and then returns it.
     * @param   id    the book's id
     * @return
     */
    Optional<Book> findById(int id) throws DaoException;
    /**
     * Finds a Book by its name and then returns it.
     * @param   name    the book's name
     * @return
     */
    List<Book> findByName(String name) throws DaoException;
    /**
     * Finds a Book by its author and then returns it.
     * @param   name    the book's author
     * @return
     */
    List<Book> findByAuthor(String name) throws DaoException;
}

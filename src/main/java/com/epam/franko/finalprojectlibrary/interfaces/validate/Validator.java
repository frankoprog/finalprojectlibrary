package com.epam.franko.finalprojectlibrary.interfaces.validate;

import com.epam.franko.finalprojectlibrary.exceptions.ValidationException;

/**
 * The interface Validator.
 *
 * @param <T> the type parameter
 */
public interface Validator<T> {
    /**
     * Is valid entity.
     *
     * @param entity the entity
     * @return boolean true or false.
     */
    boolean isValid(T entity) throws ValidationException;
}
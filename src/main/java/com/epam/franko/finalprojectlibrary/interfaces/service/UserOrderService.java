package com.epam.franko.finalprojectlibrary.interfaces.service;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.UserOrder;

import java.util.List;
import java.util.Optional;

public interface UserOrderService {
    /**
     * Find list of all user_orders
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<UserOrder> findAll() throws ServiceException;

    /**
     * Find user_order by user_order_id.
     *
     * @param id the user_order_Id
     * @return finds user_user_order by user_orderId
     * @throws ServiceException the service exception
     */
    Optional<UserOrder> findByOrderId(Integer id) throws ServiceException;

    /**
     * @param userId user's id
     * @return finds user_user_order by userId
     * @throws ServiceException
     */
    List<UserOrder> findByUserId(Integer userId) throws ServiceException;

    /**
     * @param bookId book's id
     * @return finds user_user_order by bookId
     * @throws ServiceException
     */
    List<UserOrder> findByBookId(Integer bookId) throws ServiceException;

    /**
     * Save user_order
     *
     * @param userOrder the user_order
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer save(UserOrder userOrder) throws ServiceException;

    /**
     * update Order
     *
     * @param userOrder
     * @throws ServiceException
     */
    void update(UserOrder userOrder) throws ServiceException;

    /**
     * Delete user_order by orderId
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    void delete(Integer id) throws ServiceException;
}

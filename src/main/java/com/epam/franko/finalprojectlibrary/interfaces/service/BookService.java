package com.epam.franko.finalprojectlibrary.interfaces.service;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.model.User;

import java.util.List;
import java.util.Optional;

public interface BookService {
    /**
     * Find list of all books
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Book> findAll() throws ServiceException;

    /**
     * Find book by id.
     *
     * @param id the id
     * @return the book
     * @throws ServiceException the service exception
     */
    Optional<Book> findById(Integer id) throws ServiceException;

    /**
     * Check if book exist in database
     *
     * @param name the name
     * @return the boolean
     * @throws ServiceException the service exception
     */
    boolean isExist(String name) throws ServiceException;

    /**
     * Find user by author.
     *
     * @param author the author
     * @return the book
     * @throws ServiceException the service exception
     */
    List<Book> findByAuthor(String author) throws ServiceException;

    /**
     * @param name
     * @return
     * @throws ServiceException
     */
    List<Book> findByName(String name) throws ServiceException;

    /**
     * Save book
     *
     * @param book the book
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer save(Book book) throws ServiceException;

    /**
     * @param book
     * @throws ServiceException
     */
    void update(Book book) throws ServiceException;

    /**
     * Delete book by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    void delete(Integer id) throws ServiceException;
}

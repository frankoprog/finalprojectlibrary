package com.epam.franko.finalprojectlibrary.interfaces.service;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.NumberOfBooks;

import java.util.List;
import java.util.Optional;

public interface NumberOfBooksService {
    /**
     * Find list of all numberOfBooks
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<NumberOfBooks> findAll() throws ServiceException;

    /**
     * Find numberOfBook by book_id.
     *
     * @param id the book_id
     * @return numberOfBook
     * @throws ServiceException the service exception
     */
    Optional<NumberOfBooks> findById(Integer id) throws ServiceException;

    /**
     * Save numberOfBooks
     *
     * @param numberOfBooks the numberOfBooks
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer save(NumberOfBooks numberOfBooks) throws ServiceException;

    /**
     * Delete numberOfBooks by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    void delete(Integer id) throws ServiceException;
}

package com.epam.franko.finalprojectlibrary.interfaces.dao;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.UserInfo;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.util.Optional;

public interface UserInfoDAO extends DAO<UserInfo> {
    /**
     * Find userInfo by user_id.
     *
     * @param id the user_id
     * @return numberOfBook
     * @throws DaoException
     */
    Optional<UserInfo> findByUserId(int id) throws DaoException;
}

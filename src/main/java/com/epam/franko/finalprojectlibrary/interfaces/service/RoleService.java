package com.epam.franko.finalprojectlibrary.interfaces.service;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.Role;

import java.util.List;
import java.util.Optional;

/**
 * The interface User service.
 */
public interface RoleService {
    /**
     * Find list of all roles
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Role> findAll() throws ServiceException;

    /**
     * Find role by id.
     *
     * @param id the id
     * @return the role
     * @throws ServiceException the service exception
     */
    Optional<Role> findById(Integer id) throws ServiceException;

    /**
     * Save role
     *
     * @param role the role
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer save(Role role) throws ServiceException;

    /**
     * Delete role by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    void delete(Integer id) throws ServiceException;
}

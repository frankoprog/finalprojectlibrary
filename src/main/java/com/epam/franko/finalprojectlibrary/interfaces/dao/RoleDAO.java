package com.epam.franko.finalprojectlibrary.interfaces.dao;


import com.epam.franko.finalprojectlibrary.model.Role;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.util.Optional;

public interface RoleDAO extends DAO<Role> {
    /**
     * Find role by id.
     *
     * @param id the id
     * @return the role
     * @throws DaoException
     */
    Optional<Role> findById(int id) throws DaoException;

}

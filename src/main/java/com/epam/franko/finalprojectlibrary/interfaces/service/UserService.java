package com.epam.franko.finalprojectlibrary.interfaces.service;


import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.User;

import java.util.List;
import java.util.Optional;

/**
 * The interface User service.
 */
public interface UserService {
    /**
     * Find list of all users
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<User> findAll() throws ServiceException;

    /**
     * Find user by id.
     *
     * @param id the id
     * @return the user
     * @throws ServiceException the service exception
     */
    Optional<User> findById(Integer id) throws ServiceException;

    /**
     * Check if user exist in database
     *
     * @param email the email
     * @return the boolean
     * @throws ServiceException the service exception
     */
    boolean isExist(String email) throws ServiceException;

    /**
     * Find user by email and password.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     * @throws ServiceException the service exception
     */
    Optional<User> findByEmailAndPassword(String email, String password) throws ServiceException;

    /**
     * Find user by role
     * @param role_id role id
     * @return
     * @throws ServiceException
     */
    List<User> findByRoleId(int role_id) throws ServiceException;

    /**
     * Save user
     *
     * @param user the user
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer save(User user) throws ServiceException;

    /**
     * Delete user by id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    void delete(Integer id) throws ServiceException;

    /**
     * Register new user in database
     *
     * @param name     new users name
     * @param surname  new users surname
     * @param email    new users email
     * @param phone    new users phone
     * @param password new users password
     * @return
     * @throws ServiceException
     */
    boolean register(String name, String surname, String email, String phone, String password) throws ServiceException;
}

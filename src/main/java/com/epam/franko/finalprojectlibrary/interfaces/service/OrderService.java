package com.epam.franko.finalprojectlibrary.interfaces.service;

import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.model.NumberOfBooks;
import com.epam.franko.finalprojectlibrary.model.Order;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    /**
     * Find list of all orders
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Order> findAll() throws ServiceException;

    /**
     * Find order by user_id.
     *
     * @param id the user_id
     * @return numberOfBook
     * @throws ServiceException the service exception
     */
    Optional<Order> findById(Integer id) throws ServiceException;

    /**
     * @param status
     * @return
     * @throws ServiceException
     */
    List<Order> findByStatus(String status) throws ServiceException;

    /**
     * Save order
     *
     * @param order the order
     * @return the integer
     * @throws ServiceException the service exception
     */
    Integer save(Order order) throws ServiceException;

    /**
     * update Order
     *
     * @param order
     * @throws ServiceException
     */
    void update(Order order) throws ServiceException;

    /**
     * Delete order by user_id
     *
     * @param id the id
     * @throws ServiceException the service exception
     */
    void delete(Integer id) throws ServiceException;
}

package com.epam.franko.finalprojectlibrary.interfaces.dao;

import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.model.UserOrder;

import java.util.List;
import java.util.Optional;

public interface UserOrderDAO extends DAO<UserOrder> {
    /**
     * @param id
     * @return
     * @throws DaoException
     */
    List<UserOrder> findByUserId(int id) throws DaoException;

    /**
     * @param id
     * @return
     * @throws DaoException
     */
    List<UserOrder> findByBookId(int id) throws DaoException;

    /**
     * @param id
     * @return
     * @throws DaoException
     */
    Optional<UserOrder> findByOrderId(int id) throws DaoException;
}

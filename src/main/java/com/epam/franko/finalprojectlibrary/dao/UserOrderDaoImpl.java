package com.epam.franko.finalprojectlibrary.dao;

import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.UserOrderDAO;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.model.UserOrder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserOrderDaoImpl extends BaseDaoImpl implements UserOrderDAO {
    //SQL Statements
    private static final String FIND_BY_ORDER_ID = "SELECT * FROM `userorder` WHERE `orderId`=?";
    private static final String FIND_BY_USER_ID = "SELECT * FROM `userorder` WHERE `userId`=?";
    private static final String FIND_BY_BOOK_ID = "SELECT * FROM `userorder` WHERE `bookId`=?";
    private static final String FIND_ALL = "SELECT * FROM `userorder`";
    private static final String SAVE_USER_ORDER = "INSERT INTO `userorder` (`orderId`,`userId`,`bookId`) VALUES (?,?,?)";
    private static final String UPDATE_USER_ORDER = "UPDATE `userorder` SET `userId`=?,`bookId`=? WHERE `orderId`=?";
    private static final String DELETE_USER_ORDER = "DELETE FROM `userorder` WHERE `orderId`=?";

    /**
     * finds all items
     *
     * @return list of all objects
     * @throws DaoException
     */
    @Override
    public List<UserOrder> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL); ResultSet resultSet = statement.executeQuery()) {
            List<UserOrder> userOrders = new ArrayList<>();
            while (resultSet.next()) {
               userOrders.add(new UserOrder(resultSet.getInt("orderId"), resultSet.getInt("userId")
                       , resultSet.getInt("bookId")));
            }
            ConnectionPool.getInstance().freeConnection(connection);
            return userOrders;
        } catch (SQLException e) {
            throw new DaoException(e + "Cannot find all orders");
        }
    }

    /**
     * save an object into Database
     *
     * @param item an object that will be added to database
     * @return
     * @throws DaoException
     */
    @Override
    public int save(UserOrder item) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SAVE_USER_ORDER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, item.getIdOrder());
            statement.setInt(2, item.getIdUser());
            statement.setInt(3, item.getIdBook());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            return item.getIdOrder();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * deletes an item in database
     *
     * @param id item's id that will be erased
     * @throws DaoException
     */
    @Override
    public void delete(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE_USER_ORDER)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            throw new DaoException(e + "Cannot delete order with id = " + id);
        }
    }

    /**
     * updates an item in database
     *
     * @param item item that will be updated with it's new info
     */
    @Override
    public void update(UserOrder item) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (DaoException e) {
            LOG.error(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_USER_ORDER)) {
            statement.setInt(1, item.getIdUser());
            statement.setInt(2, item.getIdBook());
            statement.executeUpdate();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException | DaoException e) {
            LOG.info(e);
        }
    }

    /**
     * @param id
     * @return
     * @throws DaoException
     */
    @Override
    public List<UserOrder> findByUserId(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_USER_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            List<UserOrder> userOrders = new ArrayList<>();
            while (resultSet.next()) {
                userOrders.add(new UserOrder(resultSet.getInt("orderId"), id, resultSet.getInt("bookId")));
            }
            return userOrders;
        } catch (SQLException e) {
            throw new DaoException(e + "Cannot find user_order by userId = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * @param id
     * @return
     * @throws DaoException
     */
    @Override
    public List<UserOrder> findByBookId(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_BOOK_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            List<UserOrder> userOrders = new ArrayList<>();
            while (resultSet.next()) {
                userOrders.add(new UserOrder(resultSet.getInt("orderId"), resultSet.getInt("userId"), id));
            }
            return userOrders;
        } catch (SQLException e) {
            throw new DaoException(e + "Cannot find user_order by bookId = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * @param id
     * @return
     * @throws DaoException
     */
    @Override
    public Optional<UserOrder> findByOrderId(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ORDER_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            UserOrder userOrder = null;
            if (resultSet.next()) {
                userOrder = new UserOrder(id, resultSet.getInt("userId"), resultSet.getInt("bookId"));
            }
            return Optional.ofNullable(userOrder);
        } catch (SQLException e) {
            throw new DaoException(e + "Cannot read user_order by orderId = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.error(e);
            }
        }
    }
}

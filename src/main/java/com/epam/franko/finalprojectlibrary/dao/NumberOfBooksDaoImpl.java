package com.epam.franko.finalprojectlibrary.dao;

import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.interfaces.dao.NumberOfBooksDAO;
import com.epam.franko.finalprojectlibrary.model.NumberOfBooks;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class contains methods that operate with NumberOfBooks Entity in DB.It extends BaseDaoImpl and implements NumberOfBooksDao
 */
public class NumberOfBooksDaoImpl extends BaseDaoImpl implements NumberOfBooksDAO {
    private static final String FIND_BY_ID = "SELECT * FROM `numberofbooks` WHERE `book_id`=?";
    private static final String FIND_ALL = "SELECT * FROM `numberofbooks`";
    private static final String SAVE_NUMBER_OF_BOOKS = "INSERT INTO `numberofbooks` (`number`) VALUES (?)";
    private static final String UPDATE_NUMBER_OF_BOOKS = "UPDATE `numberofbooks` SET `number`=? WHERE `book_id`=?";
    private static final String DELETE_NUMBER_OF_BOOKS = "DELETE FROM `numberofbooks` WHERE `book_id`=?";


    @Override
    public Optional<NumberOfBooks> findByBookId(int book_id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ID)) {
            statement.setInt(1, book_id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            NumberOfBooks numberOfBooks = null;
            if (resultSet.next()) {
                numberOfBooks = new NumberOfBooks(book_id, resultSet.getInt("number"));
            }
            return Optional.ofNullable(numberOfBooks);
        } catch (SQLException e) {
            LOG.info(e);;
            throw new DaoException(e + "Cannot read numberOfBooks by book_id = " + book_id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ////connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);;
            }
        }
    }

    @Override
    public List<NumberOfBooks> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL); ResultSet resultSet = statement.executeQuery()) {
            List<NumberOfBooks> numberOfBooks = new ArrayList<>();
            while (resultSet.next()) {
                numberOfBooks.add(new NumberOfBooks(resultSet.getInt("book_id"), resultSet.getInt("number")));
            }
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
            return numberOfBooks;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot find all numberOfBooks");
        }
    }

    @Override
    public int save(NumberOfBooks item) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SAVE_NUMBER_OF_BOOKS, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, item.getNumber());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    @Override
    public void delete(int book_id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE_NUMBER_OF_BOOKS)) {
            statement.setInt(1, book_id);
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot delete numberOfBooks with book_id = " + book_id);
        }
    }

    @Override
    public void update(NumberOfBooks item) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_NUMBER_OF_BOOKS)) {
            statement.setInt(1, item.getNumber());
            statement.setInt(2, item.getIdBook());
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            //error
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

}

package com.epam.franko.finalprojectlibrary.dao;

import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.interfaces.dao.UserInfoDAO;
import com.epam.franko.finalprojectlibrary.model.UserInfo;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class contains methods that operate with UserInfo Entity in DB.It extends BaseDaoImpl and implements UserInfo
 */
public class UserInfoDaoImpl extends BaseDaoImpl implements UserInfoDAO {
    //SQL Statements
    private static final String FIND_BY_ID = "SELECT * FROM `userinfo` WHERE `user_id`=?";
    private static final String FIND_ALL = "SELECT * FROM `userinfo`";
    private static final String SAVE_USER_INFO = "INSERT INTO `userinfo` (`user_id`,`name`,`surname`,`phone`) VALUES (?,?,?,?)";
    private static final String UPDATE_USER_INFO = "UPDATE `userinfo` SET `name`=?,`surname`=?,`phone`=? WHERE `user_id`=?";
    private static final String DELETE_USER_INFO = "DELETE FROM `userinfo` WHERE `user_id`=?";

    /**
     *
     * @param id the user_id
     * @return
     * @throws DaoException
     */
    @Override
    public Optional<UserInfo> findByUserId(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            UserInfo userInfo = null;
            if (resultSet.next()) {
                userInfo = new UserInfo(id, resultSet.getString("name"), resultSet.getString("surname")
                        , resultSet.getString("phone"));
            }
            return Optional.ofNullable(userInfo);
        } catch (SQLException e) {
            LOG.info(e);
            ;
            throw new DaoException(e + "Cannot read userInfo by id = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
                ;
            }
        }
    }

    @Override
    public List<UserInfo> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL); ResultSet resultSet = statement.executeQuery()) {
            List<UserInfo> userInfos = new ArrayList<>();
            while (resultSet.next()) {
                userInfos.add(new UserInfo(resultSet.getInt("id"), resultSet.getString("name"),
                        resultSet.getString("surname"), resultSet.getString("author")));
            }
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
            return userInfos;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot find all books");
        }
    }

    @Override
    public int save(UserInfo item) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SAVE_USER_INFO, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, item.getId());
            statement.setString(2, item.getName());
            statement.setString(3, item.getSurname());
            statement.setString(4, item.getPhone());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    @Override
    public void delete(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE_USER_INFO)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            //connection.close(); c
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot delete userInfo with id = " + id);
        }
    }

    @Override
    public void update(UserInfo item) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_USER_INFO)) {
            statement.setString(1, item.getName());
            statement.setString(2, item.getSurname());
            statement.setString(3, item.getPhone());
            statement.setInt(4, item.getId());
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            //error
        } catch (DaoException e) {
            LOG.info(e);
        }
    }
}

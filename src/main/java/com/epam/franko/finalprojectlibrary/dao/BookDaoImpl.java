package com.epam.franko.finalprojectlibrary.dao;

import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.interfaces.dao.BookDAO;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class contains methods that operate with Book Entity in DB.It extends BaseDaoImpl and implements BookDao
 */
public class BookDaoImpl extends BaseDaoImpl implements BookDAO {
    private static final String FIND_BY_ID = "SELECT * FROM `book` WHERE `id`=?";
    private static final String FIND_BY_NAME = "SELECT * FROM `book` WHERE `name`=?";
    private static final String FIND_BY_AUTHOR = "SELECT * FROM `book` WHERE `author`=?";
    private static final String FIND_ALL = "SELECT * FROM `book`";
    private static final String SAVE_BOOK = "INSERT INTO `book` (`name`,`author`,`publisherName`,`synopsis`,`photo`) VALUES (?,?,?,?,?)";
    private static final String UPDATE_BOOK = "UPDATE `book` SET `name`=?," +
            "`author`=?,`publisherName`=?,`synopsis`=?,`photo`=? WHERE `id`=?";
    private static final String DELETE_BOOK = "DELETE FROM `book` WHERE `id`=?";

    /**
     * Finds a Book by its id and then returns it.
     *
     * @param id the book's id
     * @return
     */
    @Override
    public Optional<Book> findById(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            Book book = null;
            if (resultSet.next()) {
                book = new Book(id, resultSet.getString("name"), resultSet.getString("author"),
                        resultSet.getString("publisherName"), resultSet.getString("synopsis"),
                        resultSet.getString("photo"));
            }
            return Optional.ofNullable(book);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read book by ID = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * Finds a Book by its name and then returns it.
     *
     * @param name the book's name
     * @return
     */
    @Override
    public List<Book> findByName(String name) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_NAME)) {
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            List<Book> books = new ArrayList<>();
            while (resultSet.next()) {
                books.add(new Book(resultSet.getInt("id"), name, resultSet.getString("author"),
                        resultSet.getString("publisherName"), resultSet.getString("synopsis"),
                        resultSet.getString("photo")));
            }
            return books;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read book by Name = " + name);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * Finds a Book by its author and then returns it.
     *
     * @param author the book's author
     * @return
     */
    @Override
    public List<Book> findByAuthor(String author) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_AUTHOR)) {
            statement.setString(1, author);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            List<Book> books = new ArrayList<>();
            while (resultSet.next()) {
                books.add(new Book(resultSet.getInt("id"), resultSet.getString("name"), author,
                        resultSet.getString("publisherName"), resultSet.getString("synopsis"),
                        resultSet.getString("photo")));
            }
            return books;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read book by Author = " + author);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * finds all books
     *
     * @return list of all books
     * @throws DaoException
     */
    @Override
    public List<Book> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL); ResultSet resultSet = statement.executeQuery()) {
            List<Book> books = new ArrayList<>();
            while (resultSet.next()) {
                books.add(new Book(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("author"),
                        resultSet.getString("publisherName"), resultSet.getString("synopsis"),
                        resultSet.getString("photo")));
            }
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
            return books;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot find all books");
        }

    }

    /**
     * save a book into Database
     *
     * @param item a book that will be added to database
     * @return
     * @throws DaoException
     */
    @Override
    public int save(Book item) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SAVE_BOOK, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, item.getName());
            statement.setString(2, item.getAuthor());
            statement.setString(3, item.getPublisherName());
            statement.setString(4, item.getSynopsis());
            statement.setString(5, item.getPhoto());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * deletes a book from database
     *
     * @param id book's id that will be erased
     * @throws DaoException
     */
    @Override
    public void delete(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE_BOOK)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot delete book with id = " + id);
        }
    }

    /**
     * updates a book in database
     *
     * @param item book that will be updated with its new info
     */
    @Override
    public void update(Book item) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_BOOK)) {
            statement.setString(1, item.getName());
            statement.setString(2, item.getAuthor());
            statement.setString(3, item.getPublisherName());
            statement.setString(4, item.getSynopsis());
            statement.setString(5, item.getPhoto());
            statement.setInt(6, item.getId());
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            //error
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }
}

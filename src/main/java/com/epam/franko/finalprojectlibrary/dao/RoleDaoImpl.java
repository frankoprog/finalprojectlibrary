package com.epam.franko.finalprojectlibrary.dao;

import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.interfaces.dao.RoleDAO;
import com.epam.franko.finalprojectlibrary.model.Role;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 * This class contains methods that operate with Role Entity in DB.It extends BaseDaoImpl and implements RoleDao
 */
public class RoleDaoImpl extends BaseDaoImpl implements RoleDAO {
    private static final String FIND_BY_ID = "SELECT * FROM `role` WHERE `id`=?";
    private static final String FIND_ALL = "SELECT * FROM `role`";
    private static final String SAVE_ROLE = "INSERT INTO `role` (`role`) VALUES (?)";
    private static final String UPDATE_ROLE = "UPDATE `role` SET `role`=? WHERE `id`=?";
    private static final String DELETE_ROLE = "DELETE FROM `role` WHERE `id`=?";

    /**
     * Find role by id.
     *
     * @param id the id
     * @return the role
     * @throws DaoException
     */
    @Override
    public Optional<Role> findById(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            Role role = null;
            if (resultSet.next()) {
                role = new Role(id, resultSet.getString("role"));
            }
            return Optional.ofNullable(role);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read role by ID = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }
    /**
     * finds all roles
     * @return list of all roles
     * @throws DaoException
     */
    @Override
    public List<Role> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL); ResultSet resultSet = statement.executeQuery()) {
            List<Role> roles = new ArrayList<>();
            Role role;
            while (resultSet.next()) {
                role = new Role(resultSet.getInt("id"),
                        resultSet.getString("role"));
                roles.add(role);
            }
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
            return roles;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot find all roles");
        }

    }
    /**
     * save a role into Database
     * @param item a role that will be added to database
     * @return
     * @throws DaoException
     */
    @Override
    public int save(Role item) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SAVE_ROLE, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, item.getName());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }
    /**
     * deletes a role from database
     * @param id role's id that will be erased
     * @throws DaoException
     */
    @Override
    public void delete(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ROLE)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot delete role with id = " + id);
        }
    }
    /**
     * updates a role in database
     * @param item role that will be updated with its new info
     */

    @Override
    public void update(Role item) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_ROLE)) {
            statement.setInt(1, item.getId());
            statement.setString(2, item.getName());
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            //error
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }
}

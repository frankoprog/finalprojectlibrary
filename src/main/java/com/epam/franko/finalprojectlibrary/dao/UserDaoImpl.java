package com.epam.franko.finalprojectlibrary.dao;

import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.interfaces.dao.UserDAO;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class contains methods that operate with User Entity in DB.It extends BaseDaoImpl and implements UserDao
 */
public class UserDaoImpl extends BaseDaoImpl implements UserDAO {
    //SQL Statements
    private static final String FIND_BY_ID = "SELECT * FROM `user` WHERE `id`=?";
    private static final String FIND_BY_EMAIL = "SELECT * FROM `user` WHERE `email`=?";
    private static final String FIND_BY_ROLE = "SELECT * FROM `user` WHERE `role_id`=?";
    private static final String FIND_ALL = "SELECT * FROM `user`";
    private static final String FIND_BY_EMAIL_AND_PASSWORD = "SELECT * FROM `user` WHERE `email`=? AND `password`=?";
    private static final String SAVE_USER = "INSERT INTO `user` (`email`,`password`,`role_id`) VALUES (?,?,?)";
    private static final String UPDATE_USER = "UPDATE `user` SET `email`=?,`password`=?,`role_id`=? WHERE `id`=?";
    private static final String DELETE_USER = "DELETE FROM `user` WHERE `id`=?";

    /**
     * Find user by id.
     *
     * @param id the id
     * @return the user
     * @throws DaoException
     */
    @Override
    public Optional<User> findById(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            User user = null;
            if (resultSet.next()) {
                user = new User(id, resultSet.getString("email"),
                        resultSet.getString("password"), resultSet.getInt("role_id"));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read user by id = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ////connection.close(); 
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * Find user by email.
     *
     * @param email user's email
     * @return the user
     * @throws DaoException
     */
    @Override
    public Optional<User> findByEmail(String email) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_EMAIL)) {
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            User user = null;
            if (resultSet.next()) {
                user = new User(resultSet.getInt("id"), resultSet.getString("email"),
                        resultSet.getString("password"), resultSet.getInt("role_id"));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read user by email = " + email);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close(); 
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * Find user by email and password.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     * @throws DaoException
     */
    @Override
    public Optional<User> findByEmailAndPassword(String email, String password) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_EMAIL_AND_PASSWORD)) {
            statement.setString(1, email);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            User user = null;
            if (resultSet.next()) {
                user = new User(resultSet.getInt("id"), email,
                        password, resultSet.getInt("role_id"));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read user by email = " + email + " and password = " + password);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * Find user by role
     *
     * @param role_id role id
     * @return
     * @throws DaoException
     */
    @Override
    public List<User> findByRole(int role_id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ROLE)) {
            statement.setInt(1, role_id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(new User(resultSet.getInt("id"), resultSet.getString("email"),
                        resultSet.getString("password"), role_id));
            }
            return users;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot find user by roleId = " + role_id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * finds all users
     *
     * @return list of all users
     * @throws DaoException
     */
    @Override
    public List<User> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL); ResultSet resultSet = statement.executeQuery()) {
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                users.add(new User(resultSet.getInt("id"), resultSet.getString("email"),
                        resultSet.getString("password"), resultSet.getInt("role_id")));
            }
            //connection.close(); 
            ConnectionPool.getInstance().freeConnection(connection);
            return users;
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot find all users");
        }
    }

    /**
     * save a user into Database
     *
     * @param item a user that will be added to database
     * @return
     * @throws DaoException
     */
    @Override
    public int save(User item) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SAVE_USER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, item.getEmail());
            statement.setString(2, item.getPassword());
            statement.setInt(3, item.getRoleId());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }

    /**
     * deletes a user from database
     *
     * @param id an id of user that will be erased
     * @throws DaoException
     */
    @Override
    public void delete(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE_USER)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot delete user with id = " + id);
        }
    }

    /**
     * updates a user in database
     *
     * @param item user that will be updated with its new info
     */
    @Override
    public void update(User item) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_USER)) {
            statement.setString(1, item.getEmail());
            statement.setString(2, item.getPassword());
            statement.setInt(3, item.getRoleId());
            statement.executeUpdate();
            //connection.close();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            //error
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

}

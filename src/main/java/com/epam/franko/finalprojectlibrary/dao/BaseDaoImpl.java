package com.epam.franko.finalprojectlibrary.dao;

import org.apache.log4j.Logger;

/**
 * Base Dao class that contains LOGGER .
 */
abstract public class BaseDaoImpl  {
    public static final Logger LOG = Logger.getLogger(BaseDaoImpl.class);
}

package com.epam.franko.finalprojectlibrary.dao;

import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.dao.OrderDAO;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 * This class contains methods that operate with Order Entity in DB.It extends BaseDaoImpl and implements OrderDao
 */
public class OrderDaoImpl extends BaseDaoImpl implements OrderDAO {
    private static final String FIND_BY_ID = "SELECT * FROM `order` WHERE `id`=?";
    private static final String FIND_BY_STATUS = "SELECT * FROM `order` WHERE `status`=?";
    private static final String FIND_ALL = "SELECT * FROM `order`";
    private static final String SAVE_ORDER = "INSERT INTO `order` (`order_due`,`status`) VALUES (?,?)";
    private static final String UPDATE_ORDER = "UPDATE `order` SET `order_due`=?,`status`=? WHERE `id`=?";
    private static final String DELETE_ORDER = "DELETE FROM `order` WHERE `id`=?";

    /**
     * Find order by user_id.
     *
     * @param id the user_id
     * @return numberOfBook
     * @throws DaoException,SQLException
     */
    @Override
    public Optional<Order> findById(int id) throws DaoException, SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            Order order = null;
            if (resultSet.next()) {
                order = new Order(id, resultSet.getDate("order_due"), resultSet.getString("status"));
            }
            return Optional.ofNullable(order);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot read order by id = " + id);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                //connection.close();
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }
    /**
     * @param status
     * @return
     * @throws ServiceException
     */
    @Override
    public List<Order> findByStatus(String status) throws DaoException, SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_BY_STATUS)) {
            statement.setString(1, status);
            resultSet = statement.executeQuery();
            if (resultSet == null) throw new DaoException();
            List<Order> orders = new ArrayList<>();
            while (resultSet.next()) {
                orders.add(new Order(resultSet.getInt("id"), resultSet.getDate("order_due"),
                        status));
            }
            return orders;
        } catch (SQLException e) {
            throw new DaoException(e + "Cannot find orders with status = " + status);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }
    /**
     * finds all orders
     * @return list of all orders
     * @throws DaoException
     */
    @Override
    public List<Order> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL); ResultSet resultSet = statement.executeQuery()) {
            List<Order> orders = new ArrayList<>();
            while (resultSet.next()) {
                orders.add(new Order(resultSet.getInt("id"), resultSet.getDate("order_due"),
                        resultSet.getString("status")));
            }
            ConnectionPool.getInstance().freeConnection(connection);
            return orders;
        } catch (SQLException e) {
            throw new DaoException(e + "Cannot find all orders");
        }
    }
    /**
     * save an order into Database
     * @param item an order that will be added to database
     * @return
     * @throws DaoException
     */
    @Override
    public int save(Order item) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SAVE_ORDER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setDate(1, (Date) item.getOrderDue());
            statement.setString(2, item.getStatus());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                ConnectionPool.getInstance().freeConnection(connection);
            } catch (SQLException e) {
                LOG.info(e);
            }
        }
    }
    /**
     * deletes an order in database
     * @param id item's id that will be erased
     * @throws DaoException
     */
    @Override
    public void delete(int id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ORDER)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException e) {
            LOG.info(e);
            throw new DaoException(e + "Cannot delete order with id = " + id);
        }
    }
    /**
     * updates an oder in database
     * @param item order that will be updated with its new info
     */

    @Override
    public void update(Order item) {
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (DaoException e) {
            LOG.error(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_ORDER)) {
            statement.setDate(1, (Date) item.getOrderDue());
            statement.setString(2, item.getStatus());
            statement.setInt(3, item.getId());
            statement.executeUpdate();
            ConnectionPool.getInstance().freeConnection(connection);
        } catch (SQLException | DaoException e) {
            LOG.error(e);
        }
    }

}

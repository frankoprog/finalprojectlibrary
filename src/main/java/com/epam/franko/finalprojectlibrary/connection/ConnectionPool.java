package com.epam.franko.finalprojectlibrary.connection;


import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import org.apache.log4j.Logger;;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Connection Pool class that contains method which operates with connections.
 */
public class ConnectionPool {
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);
    private static ConnectionPool instance;
    private BlockingQueue<Connection> availableConnections;
    private BlockingQueue<Connection> usedConnections;
    private static ReentrantLock lock = new ReentrantLock();
    private volatile boolean isInit = false;

    /**
     * Constructor
     */
    private ConnectionPool() {

        try {
            init();
        } catch (DaoException e) {
          LOG.info(e);;
        }
    }

    /**
     * Method that starts when a ConnectionPool is created.It connects to Database using credentials
     * and also sets a limit to existing connections.
     * @throws DaoException
     */
    public void init() throws DaoException {
        try {
            lock.lock();
            if (!isInit) {
                ResourceBundle dbProperty = ResourceBundle.getBundle("db");
                int poolSize = Integer.parseInt(dbProperty.getString("db.number"));
                availableConnections = new ArrayBlockingQueue<>(poolSize);
                usedConnections = new ArrayBlockingQueue<>(poolSize);

                try {
                    Class.forName(dbProperty.getString("db.driver"));
                    for (int i = 0; i < poolSize; i++) {
                        Connection connection = DriverManager.getConnection(
                                dbProperty.getString("db.url") + dbProperty.getString("db.name"),
                                dbProperty.getString("db.login"),
                                dbProperty.getString("db.password")
                        );

                        availableConnections.add(connection);
                    }
                    isInit = true;

                } catch (ClassNotFoundException | SQLException e) {
                    throw new DaoException("Cannot init a pool", e);
                }
            } else {
                throw new DaoException("Try to init already init pool");
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Destroys an instance of ConnectionPool if created
     * @throws DaoException
     */
    public void destroy() throws DaoException {
        try {
            lock.lock();
            if (isInit) {
                lock.lock();
                try {
                    for (Connection connection : availableConnections) {
                        connection.close();
                    }
                    availableConnections.clear();
                    for (Connection connection : usedConnections) {
                        connection.close();
                    }
                    usedConnections.clear();
                    isInit = false;

                } catch (SQLException e) {
                    throw new DaoException("Cannot destroy a pool", e);
                }
            } else {
                throw new DaoException("Try to destroy not init pool");
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Method that allows other methods to use this class commands without creating new objects
     * @return Connection Pool's instance
     */
    public static ConnectionPool getInstance() {
       if(instance == null) {
           instance = new ConnectionPool();
           return instance;
       }
       return instance;
    }

    /**
     * Method that gets a free connection from existing pool.
     * @return Connection a free connection that was taken from the pool
     * @throws DaoException
     */
    public Connection getConnection() throws DaoException {
        try {
            Connection connection = availableConnections.take();
            usedConnections.add(connection);
            return connection;
        } catch(InterruptedException e){
            throw new DaoException("Taking interrupted connection", e);
        }
    }

    /**
     * Method that returns a connection to the pool when once done
     * @param connection used connection
     * @throws DaoException
     */
    public void freeConnection(Connection connection) throws DaoException {
        if(usedConnections.contains(connection)){
            try {
                usedConnections.remove(connection);
                availableConnections.put(connection);
            } catch(InterruptedException e){
                throw new DaoException("Can't free connection", e);
            }
        }
        else LOG.info("There are no used connections");
    }
}

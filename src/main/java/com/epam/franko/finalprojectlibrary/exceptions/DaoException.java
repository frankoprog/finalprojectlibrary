package com.epam.franko.finalprojectlibrary.exceptions;

import org.apache.log4j.Logger;

public class DaoException extends Exception {
    private static final Logger LOG = Logger.getLogger(DaoException.class);
    public DaoException() {
    }

    public DaoException(String message) {
        LOG.error(message);
    }

    public DaoException(String message, Throwable cause) {
        LOG.info(message, cause);
    }
}

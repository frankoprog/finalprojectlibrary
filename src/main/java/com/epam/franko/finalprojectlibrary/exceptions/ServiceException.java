package com.epam.franko.finalprojectlibrary.exceptions;

import org.apache.log4j.Logger;

public class ServiceException extends Throwable {
    private static final Logger LOG = Logger.getLogger(ServiceException.class);
    public ServiceException() {
    }

    public ServiceException(String message) {
        LOG.error(message);
    }

    public ServiceException(String message, Throwable cause) {
        LOG.error(message, cause);
    }
}

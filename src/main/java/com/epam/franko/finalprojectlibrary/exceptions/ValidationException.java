package com.epam.franko.finalprojectlibrary.exceptions;


import org.apache.log4j.Logger;

public class ValidationException extends Throwable {
    private static final Logger LOG = Logger.getLogger(ValidationException.class);
    public ValidationException() {
    }

    public ValidationException(String message) {
        LOG.error(message);
    }

    public ValidationException(String message, Throwable cause) {
        LOG.error(message, cause);
    }
}

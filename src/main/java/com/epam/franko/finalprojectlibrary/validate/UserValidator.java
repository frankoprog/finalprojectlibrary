package com.epam.franko.finalprojectlibrary.validate;

import com.epam.franko.finalprojectlibrary.exceptions.ValidationException;
import com.epam.franko.finalprojectlibrary.interfaces.validate.Validator;
import com.epam.franko.finalprojectlibrary.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator implements Validator<User> {
    private static final Logger logger = LogManager.getLogger(UserValidator.class);
    private static final String EMAIL = "^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$";
    private static final String PASSWORD = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$";

    /**
     * Is valid entity.
     *
     * @param entity the entity
     * @return boolean true or false.
     */
    @Override
    public boolean isValid(User entity) throws ValidationException {
        if (entity == null) {
            String message = "Entity is null";
            logger.info("Entity is null");
            throw new ValidationException(message);
        }
        Pattern pattern = Pattern.compile(EMAIL);

        Matcher matcher = pattern.matcher(entity.getEmail());
        if (!matcher.find()) {
            String message = "Email is invalid";
            logger.info(message);
            throw new ValidationException(message);
        }
        if (entity.getPassword() == null) {
            String message = "Password is null";
            logger.info(message);
            throw new ValidationException(message);
        }
        pattern = Pattern.compile(PASSWORD);

        matcher = pattern.matcher(entity.getPassword());
        if (!matcher.find()) {
            String message = "Password is null";
            logger.info(message);
            throw new ValidationException(message);
        }
        return true;
    }
}

package com.epam.franko.finalprojectlibrary.command.action.orders;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserOrderService;
import com.epam.franko.finalprojectlibrary.service.UserOrderServiceImpl;

import javax.servlet.http.HttpServletResponse;


public class DeleteOrderCommand implements Command {
    private static final String COMMAND = "command=orders";
    private static final String BOOK_ID = "bookId";
    private static final String BOOKS = "books";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        int orderId = Integer.parseInt(requestContext.getRequestParameter("orderIdDelete"));
        try {
            UserOrderService userOrderService = new UserOrderServiceImpl();
            userOrderService.delete(orderId);

        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(COMMAND, CommandType.REDIRECT);

    }
}

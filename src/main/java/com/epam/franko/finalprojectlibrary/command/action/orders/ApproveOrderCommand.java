package com.epam.franko.finalprojectlibrary.command.action.orders;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserOrderService;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.model.UserOrder;
import com.epam.franko.finalprojectlibrary.service.OrderServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserOrderServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public class ApproveOrderCommand implements Command {
    private static final String COMMAND = "command=orders";
    private static final String BOOK_ID = "bookId";
    private static final String BOOKS = "books";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        int orderId = Integer.parseInt(requestContext.getRequestParameter("orderId"));
        try {
            OrderService orderService = new OrderServiceImpl();
            Optional<Order> userOrder = orderService.findById(orderId);
            if (userOrder.isPresent()) {
                userOrder.get().setStatus("Lend");
                Date date = new Date(new java.util.Date().getTime() + 1209600000);
                userOrder.get().setOrderDue(date);
                orderService.update(userOrder.get());
            }

        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(COMMAND, CommandType.REDIRECT);

    }
}

package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class SearchBookCommand implements Command {
    private static final String COMMAND = "/WEB-INF/jsp/catalog.jsp";
    private static final String SEARCH_BOOKS = "searchedBooks";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        String text = requestContext.getRequestParameter("searchText");
        byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
        text = new String(bytes, StandardCharsets.UTF_8);
        try {
            BookService bookService = new BookServiceImpl();
            List<Book> books = bookService.findByAuthor(text);
            books.addAll(bookService.findByName(text));
            requestContext.addRequestAttribute(SEARCH_BOOKS, books);
        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(COMMAND, CommandType.FORWARD);
    }
}

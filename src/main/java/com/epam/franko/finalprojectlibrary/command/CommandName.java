package com.epam.franko.finalprojectlibrary.command;

/**
 * A Class that contains all command's names by which it can be interacted in JSP files/
 */
public class CommandName {
    public static final String HOME_COMMAND = "home";
    public static final String DEFAULT_COMMAND = "default";
    public static final String CABINET_PAGE_COMMAND = "cabinetPage";
    public static final String READERS_COMMAND = "readers";
    public static final String READERS_ORDERS_COMMAND = "readersOrders";
    public static final String ADD_ORDER_COMMAND = "addOrder";
    public static final String ORDERS_COMMAND = "orders";
    public static final String LOG_IN_COMMAND = "login";
    public static final String CATALOG_PAGE_COMMAND = "catalog";
    public static final String SEARCH_CATALOG_COMMAND = "searchBooks";
    public static final String CHECK_LOGIN_COMMAND = "checkLogin";
    public static final String LOG_OUT_COMMAND = "logout";
    public static final String SIGN_UP_COMMAND = "signUp";
    public static final String SIGN_UP_PAGE_COMMAND = "signUpPage";
    public static final String APPROVE_ORDER_COMMAND = "approveOrder";
    public static final String DELETE_ORDER_COMMAND = "deleteOrder";
    public static final String ADD_BOOK_PAGE_COMMAND = "addBookPage";
    public static final String ADD_BOOK_COMMAND = "addBook";
    public static final String DELETE_BOOK_COMMAND = "deleteBook";
    public static final String EDIT_BOOK_PAGE_COMMAND = "editBookPage";
    public static final String EDIT_BOOK_COMMAND = "editBook";
    public static final String LIBRARIANS_PAGE_COMMAND = "librariansPage";
    public static final String PAY_FINE_COMMAND = "payFine";


}

package com.epam.franko.finalprojectlibrary.command.action.autorization;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserService;
import com.epam.franko.finalprojectlibrary.service.UserServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
/**
 * Class that contains methods to execute SignUp command
 */
public class SignUpCommand implements Command {
    private static final String LOG_UP_PAGE = "WEB-INF/jsp/register.jsp";
    private static final String ERROR_PAGE = "WEB-INF/jsp/error.jsp";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String PHONE = "phone";
    private static final String MESSAGE = "message";
    private static final String ERROR = "error";
    private static final String OK = "ok";

    /**
     * Method that registers a new user in database.Checks email if it already exists in Database
     * and gives error if it exists or creates a new user otherwise
     * @param helper
     * @param response
     * @return
     */
    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        String message = ERROR;

        Optional<String> email = Optional.ofNullable(requestContext.getRequestParameter(EMAIL));
        Optional<String> password = Optional.ofNullable(requestContext.getRequestParameter(PASSWORD));
        Optional<String> name = Optional.ofNullable(requestContext.getRequestParameter(NAME));
        Optional<String> surname = Optional.ofNullable(requestContext.getRequestParameter(SURNAME));
        Optional<String> phone = Optional.ofNullable(requestContext.getRequestParameter(PHONE));

        try {
            if (email.isPresent() && password.isPresent() &&
                    name.isPresent() && surname.isPresent() && phone.isPresent()) {
                    UserService userService = new UserServiceImpl();
                    boolean result = userService.register(name.get(), surname.get(), email.get(), phone.get(), DigestUtils.sha1Hex(password.get()));
                    if (result) message = OK;

            }
        } catch (ServiceException e) {
            return new CommandResult(ERROR_PAGE, CommandType.FORWARD);
        }

        requestContext.addRequestAttribute(MESSAGE, message);
        helper.updateRequest(requestContext);
        return new CommandResult(LOG_UP_PAGE, CommandType.FORWARD);
    }
}

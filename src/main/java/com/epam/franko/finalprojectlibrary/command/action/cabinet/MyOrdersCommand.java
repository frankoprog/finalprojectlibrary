//package com.epam.franko.finalprojectlibrary.command.action.cabinet;
//
//import com.epam.franko.finalprojectlibrary.command.Command;
//import com.epam.franko.finalprojectlibrary.command.CommandResult;
//import com.epam.franko.finalprojectlibrary.command.CommandType;
//import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
//import com.epam.franko.finalprojectlibrary.command.request.Requests;
//import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
//import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
//import com.epam.franko.finalprojectlibrary.model.Book;
//import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;
//
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
///**
// * Class that contains method to show
// */
//public class MyOrdersCommand implements Command {
//    private static final String PAGE = "-";
//    private static final String BOOKS = "books";
//
//    @Override
//    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
//        Requests requestContext = helper.createContext();
//        try {
//            BookService bookService = new BookServiceImpl();
//            List<Book> books = bookService.findAll();
//            requestContext.addRequestAttribute(BOOKS, books);
//        } catch (ServiceException e) {
//            LOG.info(e);;
//        }
//        helper.updateRequest(requestContext);
//        return new CommandResult(PAGE, CommandType.REDIRECT);
//    }
//}

package com.epam.franko.finalprojectlibrary.command.action.orders;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserOrderService;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.model.UserOrder;
import com.epam.franko.finalprojectlibrary.service.OrderServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserOrderServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class OrdersCommand implements Command {
    private static final String PAGE = "WEB-INF/jsp/orders.jsp";
    private static final String ORDERS = "orders";
    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        try {
            UserOrderService userOrderService = new UserOrderServiceImpl();
            List<UserOrder> orders = userOrderService.findAll();
            requestContext.addRequestAttribute(ORDERS, orders);
        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandType.FORWARD);
    }
}

package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.service.OrderServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddBookPageCommand implements Command {
    private static final String PAGE = "WEB-INF/jsp/book/addBook.jsp";
    private static final String ORDERS = "orders";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();

        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandType.FORWARD);
    }
}

package com.epam.franko.finalprojectlibrary.command.action.cabinet;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserOrderService;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.model.UserOrder;
import com.epam.franko.finalprojectlibrary.service.OrderServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserOrderServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class PayFineCommand implements Command {
    private static final String COMMAND = "command=cabinet";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        int orderId = Integer.parseInt(requestContext.getRequestParameter("orderId"));
        try {
            LOG.info("Fine entry");
            UserOrderService userOrderService = new UserOrderServiceImpl();
            OrderService orderService = new OrderServiceImpl();
            Optional<Order> order = orderService.findById(orderId);
            Optional<UserOrder> userOrder = userOrderService.findByOrderId(orderId);
            LOG.info("Fine find");
            userOrderService.delete(orderId);
            LOG.info("Fine 1");
            orderService.delete(orderId);
            LOG.info("Delete Successful");

        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(COMMAND, CommandType.REDIRECT);

    }
}

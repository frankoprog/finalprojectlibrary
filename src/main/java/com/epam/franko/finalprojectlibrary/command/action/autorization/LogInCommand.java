package com.epam.franko.finalprojectlibrary.command.action.autorization;


import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.RoleService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserService;
import com.epam.franko.finalprojectlibrary.model.Role;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.service.RoleServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Class that contains methods to execute Login command
 */
public class LogInCommand implements Command {
    private static final String HOME = "eLibrary?command=home";
    private static final String ERROR_PAGE = "WEB-INF/jsp/error/error.jsp";
    private static final String LOGIN_PAGE = "command=login";
    private static final String PASSWORD_PARAMETER = "password";
    private static final String EMAIL_PARAMETER = "email";
    private static final String USER = "user";
    private static final String ROLE = "role";
    private static final String ERROR_MESSAGE = "error";
    private static final Logger LOG = Logger.getLogger(LogInCommand.class);


    /**
     * Method that checks given Login and Password and compares it to existing ones in Database
     * and then redirects to another page if successful or to error page otherwise
     *
     * @param helper
     * @param response
     * @return
     */
    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        String password = requestContext.getRequestParameter(PASSWORD_PARAMETER);
        String login = requestContext.getRequestParameter(EMAIL_PARAMETER);
        try {
            UserService userService = new UserServiceImpl();
            password = DigestUtils.sha1Hex(password);
            Optional<User> optionalResult = userService.findByEmailAndPassword(login, password);
            if (optionalResult.isPresent()) {
                requestContext.addSessionAttribute(USER, optionalResult.get());
                LOG.info("Login Successful");
                RoleService roleService = new RoleServiceImpl();
                Optional<Role> role = roleService.findById(optionalResult.get().getRoleId());
                role.ifPresent(value -> requestContext.addSessionAttribute(ROLE, value));

                helper.updateRequest(requestContext);
                return new CommandResult(HOME, CommandType.FORWARD);
            }
        } catch (ServiceException e) {
            return new CommandResult(ERROR_PAGE, CommandType.FORWARD);
        }
        requestContext.addRequestAttribute(ERROR_MESSAGE, true);
        helper.updateRequest(requestContext);
        return new CommandResult(LOGIN_PAGE, CommandType.REDIRECT);
    }
}

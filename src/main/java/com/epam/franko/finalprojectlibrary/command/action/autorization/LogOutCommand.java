package com.epam.franko.finalprojectlibrary.command.action.autorization;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;

import javax.servlet.http.HttpServletResponse;
/**
 * Class that contains methods to execute Logout command
 */
public class LogOutCommand implements Command {
    private static final String HOME = "WEB-INF/jsp/home.jsp";
    private static final String USER = "user";
    private static final String ROLE = "role";

    /**
     * Method that logouts from user.Removes session attribute that contains user's id
     * @param helper
     * @param response
     * @return
     */
    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requests = helper.createContext();
        requests.removeSessionAttribute(USER);
        requests.removeSessionAttribute(ROLE);
        helper.updateRequest(requests);
        return new CommandResult(HOME, CommandType.FORWARD);
    }
}

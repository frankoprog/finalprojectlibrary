package com.epam.franko.finalprojectlibrary.command;

/**
 * Enum Class that contains types of Command.
 */
public enum CommandType {
    FORWARD, REDIRECT
}

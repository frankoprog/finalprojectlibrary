package com.epam.franko.finalprojectlibrary.command;


import com.epam.franko.finalprojectlibrary.command.action.cabinet.CabinetPageCommand;
import com.epam.franko.finalprojectlibrary.command.action.cabinet.PayFineCommand;
import com.epam.franko.finalprojectlibrary.command.action.catalog.*;
import com.epam.franko.finalprojectlibrary.command.action.HomePageCommand;
import com.epam.franko.finalprojectlibrary.command.action.autorization.*;
import com.epam.franko.finalprojectlibrary.command.action.librarians.LibrarinsPageCommand;
import com.epam.franko.finalprojectlibrary.command.action.orders.ApproveOrderCommand;
import com.epam.franko.finalprojectlibrary.command.action.orders.DeleteOrderCommand;
import com.epam.franko.finalprojectlibrary.command.action.orders.OrdersCommand;
import com.epam.franko.finalprojectlibrary.command.action.readers.ReadersCommand;
import com.epam.franko.finalprojectlibrary.command.action.readers.ReadersOrdersCommand;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Class that's a factory.It contains all existing commands to execute it more comfortably.
 */
public class CommandFactory {
    private static final Map<String, Command> commands = new HashMap<>();

    private CommandFactory() {
        commands.put(CommandName.HOME_COMMAND, new HomePageCommand());
        commands.put(CommandName.LOG_IN_COMMAND, new LoginPageCommand());
        commands.put(CommandName.SIGN_UP_PAGE_COMMAND, new SingUpPageCommand());
        commands.put(CommandName.CHECK_LOGIN_COMMAND, new LogInCommand());
        commands.put(CommandName.LOG_OUT_COMMAND, new LogOutCommand());
        commands.put(CommandName.CABINET_PAGE_COMMAND, new CabinetPageCommand());
        commands.put(CommandName.SIGN_UP_COMMAND, new SignUpCommand());
        commands.put(CommandName.CATALOG_PAGE_COMMAND, new CatalogPageCommand());
        commands.put(CommandName.ORDERS_COMMAND, new OrdersCommand());
        commands.put(CommandName.READERS_COMMAND, new ReadersCommand());
        commands.put(CommandName.ADD_ORDER_COMMAND, new AddOrderCommand());
        commands.put(CommandName.APPROVE_ORDER_COMMAND, new ApproveOrderCommand());
        commands.put(CommandName.DELETE_ORDER_COMMAND, new DeleteOrderCommand());
        commands.put(CommandName.READERS_ORDERS_COMMAND, new ReadersOrdersCommand());
        commands.put(CommandName.SEARCH_CATALOG_COMMAND, new SearchBookCommand());
        commands.put(CommandName.ADD_BOOK_PAGE_COMMAND, new AddBookPageCommand());
        commands.put(CommandName.ADD_BOOK_COMMAND, new AddBookCommand());
        commands.put(CommandName.DELETE_BOOK_COMMAND, new DeleteBookCommand());
        commands.put(CommandName.EDIT_BOOK_PAGE_COMMAND, new EditBookPageCommand());
        commands.put(CommandName.EDIT_BOOK_COMMAND, new EditBookCommand());
        commands.put(CommandName.LIBRARIANS_PAGE_COMMAND, new LibrarinsPageCommand());
        commands.put(CommandName.PAY_FINE_COMMAND, new PayFineCommand());
    }

    /**
     * Method that returns CommandFactory instance
     *
     * @return Command Factory's instance.
     */
    public static CommandFactory getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Method that return a command from the factory by it's name
     *
     * @param name - Command name
     * @return Command by name from the factory
     */
    public Command getCommand(String name) {
        return Optional.ofNullable(commands.get(name)).orElse(commands.get(CommandName.DEFAULT_COMMAND));
    }

    /**
     * Class that helps to store CommandFactory's instance
     */
    private static class Holder {
        static final CommandFactory INSTANCE = new CommandFactory();
    }
}

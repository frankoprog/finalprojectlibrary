package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.connection.ConnectionPool;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;
import com.epam.franko.finalprojectlibrary.service.OrderServiceImpl;

import javax.servlet.http.HttpServletResponse;

public class DeleteBookCommand implements Command {
    private static final String COMMAND = "command=catalog";
    private static final String BOOK_ID = "bookId";
    private static final String BOOKS = "books";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        int bookId = Integer.parseInt(requestContext.getRequestParameter("bookIdDelete"));
        try {
            BookService bookService = new BookServiceImpl();
            bookService.delete(bookId);

        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(COMMAND, CommandType.REDIRECT);

    }
}

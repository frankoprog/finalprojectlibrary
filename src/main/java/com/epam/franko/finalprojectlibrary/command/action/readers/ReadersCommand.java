package com.epam.franko.finalprojectlibrary.command.action.readers;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserService;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.service.UserServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class ReadersCommand implements Command {
    private static final String PAGE = "WEB-INF/jsp/readers/readers.jsp";
    private static final String READERS = "readers";
    private static final Integer ROLE_ID = 1;

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        try {
            UserService userService = new UserServiceImpl();
            List<User> users = userService.findByRoleId(ROLE_ID);
            requestContext.addRequestAttribute(READERS, users);
        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandType.FORWARD);
    }
}

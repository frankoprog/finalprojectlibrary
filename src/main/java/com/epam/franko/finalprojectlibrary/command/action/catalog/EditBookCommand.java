package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserService;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class EditBookCommand implements Command {
    private static final String COMMAND = "command=catalog";
    private static final String ERROR_PAGE = "WEB-INF/jsp/error.jsp";
    private static final String ID = "bookId";
    private static final String NAME = "bookName";
    private static final String AUTHOR = "authorName";
    private static final String PUBLISHER = "publisherName";
    private static final String BOOK_FOR_REDIRECT_EDIT = "bookIdForEditRedirect";
    private static final String BOOK_FOR_EDIT = "bookIdForEdit";
    private static final String ERROR = "error";
    private static final String OK = "ok";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();

        Optional<String> name = Optional.ofNullable(requestContext.getRequestParameter(NAME));
        Optional<String> author = Optional.ofNullable(requestContext.getRequestParameter(AUTHOR));
        Optional<String> publisher = Optional.ofNullable(requestContext.getRequestParameter(PUBLISHER));
        int id = Integer.parseInt(requestContext.getRequestParameter(ID));
        try {
            if (name.isPresent() && author.isPresent() && publisher.isPresent()) {
                BookService bookService = new BookServiceImpl();
                Optional<Book> book = bookService.findById(id);
                if (book.isPresent()) {
                    book.get().setName(name.get());
                    book.get().setAuthor(author.get());
                    book.get().setPublisherName(publisher.get());
                    bookService.update(book.get());
                }

            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        helper.updateRequest(requestContext);
        return new CommandResult(COMMAND, CommandType.REDIRECT);
    }
}

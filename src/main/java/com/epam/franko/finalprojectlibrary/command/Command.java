package com.epam.franko.finalprojectlibrary.command;

import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.dao.BaseDaoImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;

/**
 * Interface that contains Logger and a method that executes commands.
 */
public interface Command {
    public static final Logger LOG = Logger.getLogger(BaseDaoImpl.class);

    CommandResult execute(RequestManager helper, HttpServletResponse response);

}

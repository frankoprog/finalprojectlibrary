package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.interfaces.service.RoleService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserService;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.model.Role;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;
import com.epam.franko.finalprojectlibrary.service.RoleServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class AddBookCommand implements Command {
    private static final String HOME = "command=catalog";
    private static final String NAME_PARAMETER = "bookName";
    private static final String AUTHOR_PARAMETER = "authorName";
    private static final String PUBLISHER_PARAMETER = "publisherName";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        String name = requestContext.getRequestParameter(NAME_PARAMETER);
        String author = requestContext.getRequestParameter(AUTHOR_PARAMETER);
        String publisher = requestContext.getRequestParameter(PUBLISHER_PARAMETER);

        try {
            Book book = new Book(0, name, author, publisher, "", null);
            BookService bookService = new BookServiceImpl();
            bookService.save(book);
            helper.updateRequest(requestContext);
        } catch (ServiceException ex) {
            LOG.info(ex);
        }
        return new CommandResult(HOME, CommandType.REDIRECT);
    }
}

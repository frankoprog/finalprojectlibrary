package com.epam.franko.finalprojectlibrary.command.action.cabinet;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserOrderService;
import com.epam.franko.finalprojectlibrary.model.*;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;
import com.epam.franko.finalprojectlibrary.service.OrderServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserInfoServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserOrderServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.lang.management.MemoryType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Class that contains methods to redirect to Cabinet Page\.
 */
public class CabinetPageCommand implements Command {
    private static final String PROFILE = "WEB-INF/jsp/cabinet.jsp";
    private static final String EMAIL = "email";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String PHONE = "phone";
    private static final String MY_ORDERS = "myOrders";
    private static final String MY_BOOKS = "myBooks";
    private static final String MY_ORDERS_WITH = "myOrdersWith";


    /**
     * Method that redirects to Cabinet page and shows personal info(name,email ...) and existing orders
     *
     * @param helper
     * @param response
     * @return
     */
    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        User user = (User) requestContext.getSessionAttribute("user");
        if (user != null) {
            String email = user.getEmail();
            String name = null;
            String surname = null;
            String phone = null;
            try {
                UserInfoServiceImpl userInfoService = new UserInfoServiceImpl();
                Optional<UserInfo> userInfo = userInfoService.findById(user.getId());
                if (userInfo.isPresent()) {
                    name = userInfo.get().getName();
                    surname = userInfo.get().getSurname();
                    phone = userInfo.get().getPhone();
                }
                OrderService orderService = new OrderServiceImpl();
                BookService bookService = new BookServiceImpl();
                UserOrderService UserOrderService = new UserOrderServiceImpl();
                List<UserOrder> orders = UserOrderService.findByUserId(user.getId());
                List<Book> books = new ArrayList<>();
                List<Order> ordersWith = new ArrayList<>();
                for (UserOrder userOrder : orders) {
                    books.add(bookService.findById(userOrder.getIdBook()).get());
                    ordersWith.add(orderService.findById(userOrder.getIdOrder()).get());
                }
                for (Order order : ordersWith) {
                    if (!order.getStatus().equals("Pending") &&
                            new Date(System.currentTimeMillis()).getTime() - order.getOrderDue().getTime() >= TimeUnit.DAYS.toMillis(1)) {
                        order.setStatus("Overdue");
                        orderService.update(order);
                    }
                }
                requestContext.addRequestAttribute(MY_ORDERS, orders);
                requestContext.addRequestAttribute(MY_ORDERS_WITH, ordersWith);
                requestContext.addRequestAttribute(MY_BOOKS, books);
            } catch (ServiceException e) {
                LOG.info(e);
            }
            requestContext.addRequestAttribute(NAME, name);
            requestContext.addRequestAttribute(SURNAME, surname);
            requestContext.addRequestAttribute(EMAIL, email);
            requestContext.addRequestAttribute(PHONE, phone);

        }
        helper.updateRequest(requestContext);
        return new CommandResult(PROFILE, CommandType.FORWARD);
    }
}

package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.interfaces.service.OrderService;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserOrderService;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.model.Order;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.model.UserOrder;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;
import com.epam.franko.finalprojectlibrary.service.OrderServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserOrderServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class AddOrderCommand implements Command {
    private static final String COMMAND = "command=catalog";
    private static final String BOOK_ID = "bookId";
    private static final String BOOKS = "books";

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        int bookId = Integer.parseInt(requestContext.getRequestParameter("bookId"));
        User user = (User) requestContext.getSessionAttribute("user");
        if (bookId != 0) {
            try {
                OrderService orderService = new OrderServiceImpl();
                Order newOrder = new Order(0, null, "Pending");
                int orderID = orderService.save(newOrder);
                System.out.println("SMOOOTH");
                UserOrderService userOrderService = new UserOrderServiceImpl();
                UserOrder newUserOrder = new UserOrder(orderID, user.getId(), bookId);
                userOrderService.save(newUserOrder);
            } catch (ServiceException e) {
                LOG.info(e);
            }
        }
        helper.updateRequest(requestContext);
        return new CommandResult(COMMAND, CommandType.REDIRECT);
    }
}

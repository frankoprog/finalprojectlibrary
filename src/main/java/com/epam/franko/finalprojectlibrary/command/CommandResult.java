package com.epam.franko.finalprojectlibrary.command;

/**
 * A Class that contains methods and fields needed to use commands.
 */
public class CommandResult {
    private final String page;
    private final CommandType type;

    public CommandResult(String page, CommandType resultType) {
        this.page = page;
        type = resultType;
    }

    public boolean isRedirect() {
        return type == CommandType.REDIRECT;
    }

    public boolean isForward() {
        return type == CommandType.FORWARD;
    }

    public String getPage() {
        return page;
    }
}

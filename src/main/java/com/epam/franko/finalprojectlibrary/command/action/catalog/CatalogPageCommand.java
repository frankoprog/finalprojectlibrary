package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class CatalogPageCommand implements Command {
    private static final String PAGE = "WEB-INF/jsp/catalog.jsp";
    private static final String BOOKS = "books";
    public static final String NUMBER_OF_PAGES = "nuPages";
    public static final String CURRENT_PAGE = "page";
    public static final String SORT = "sort";
    public static final String GO_TO_PAGE = "goToPage";
    private static final int RECORDS_PER_PAGE = 5;

    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        try {
            int page = 1;
            String sort = null;
            if (requestContext.getRequestParameter(SORT) != null) {
                sort = requestContext.getRequestParameter(SORT);
            }
            LOG.info(sort);
            if (requestContext.getRequestParameter(CURRENT_PAGE) != null) {
                page = Integer.parseInt(requestContext.getRequestParameter(CURRENT_PAGE));
            }
            BookService bookService = new BookServiceImpl();
            List<Book> books = bookService.findAll();
            List<Book> pageBooks = new ArrayList<>();
            for (int i = ((page - 1) * RECORDS_PER_PAGE); i < Math.min(page * RECORDS_PER_PAGE, books.size()); i++) {
                pageBooks.add(books.get(i));
            }
            int numberOfPages = (books.size() + RECORDS_PER_PAGE - 1) / RECORDS_PER_PAGE;
            requestContext.addRequestAttribute(BOOKS, pageBooks);
            requestContext.addRequestAttribute(NUMBER_OF_PAGES, numberOfPages);
            requestContext.addRequestAttribute(CURRENT_PAGE, page);
        } catch (ServiceException e) {
            LOG.info(e);
        }
        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandType.FORWARD);
    }
}
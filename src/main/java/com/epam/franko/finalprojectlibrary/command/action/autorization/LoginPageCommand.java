package com.epam.franko.finalprojectlibrary.command.action.autorization;


import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;


import javax.servlet.http.HttpServletResponse;
/**
 * Class that contains methods to redirect to Login Page.
 */
public class LoginPageCommand implements Command {
    private static final String PAGE = "WEB-INF/jsp/login.jsp";

    /**
     * Method that redirects to Login page
     * @param helper
     * @param response
     * @return
     */
    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();

        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandType.FORWARD);
    }
}

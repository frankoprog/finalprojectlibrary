package com.epam.franko.finalprojectlibrary.command.action.catalog;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandResult;
import com.epam.franko.finalprojectlibrary.command.CommandType;
import com.epam.franko.finalprojectlibrary.command.request.RequestManager;
import com.epam.franko.finalprojectlibrary.command.request.Requests;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class EditBookPageCommand implements Command {
    private static final String PAGE = "WEB-INF/jsp/book/editBook.jsp";
    private static final String BOOK_FOR_EDIT = "bookId";
    private static final String ID = "bookId";
    private static final String NAME = "bookName";
    private static final String AUTHOR = "authorName";
    private static final String PUBLISHER = "publisherName";
    private static final String SYNOPSIS = "synopsisName";


    @Override
    public CommandResult execute(RequestManager helper, HttpServletResponse response) {
        Requests requestContext = helper.createContext();
        int bookId = Integer.parseInt(requestContext.getRequestParameter(BOOK_FOR_EDIT));
        Optional<Book> book = null;
        try {
            BookService bookService = new BookServiceImpl();
            book = bookService.findById(bookId);
        } catch (ServiceException e) {
            LOG.info(e);
        }
        if (book.isPresent()) {
            requestContext.addRequestAttribute(ID, bookId);
            requestContext.addRequestAttribute(NAME, book.get().getName());
            requestContext.addRequestAttribute(AUTHOR, book.get().getAuthor());
            requestContext.addRequestAttribute(PUBLISHER, book.get().getPublisherName());
            requestContext.addRequestAttribute(SYNOPSIS, book.get().getSynopsis());
        }
        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandType.FORWARD);
    }
}

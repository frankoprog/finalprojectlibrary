<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/topnav.css"/>
</head>
<body>
<fmt:setLocale value="${sessionScope.sessionLocale}"/>
<fmt:setBundle basename="langResources.langResources" var="loc"/>
<div class="topnav">
    <a class="active" href="${pageContext.request.contextPath}/eLibrary?command=home"><fmt:message bundle="${loc}"
                                                                                                   key="menu.home"/></a>
    <a href="${pageContext.request.contextPath}/eLibrary?command=catalog"><fmt:message bundle="${loc}"
                                                                                       key="menu.catalog"/></a>
    <div class="topnav-right">
        <c:choose>
            <c:when test="${sessionScope.sessionLocale == 'en'}">
                <a class="nav-link active"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sessionLocale=en">
                    EN
                </a>
                <a class="nav-link"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sessionLocale=ua">
                    UA
                </a>
            </c:when>
            <c:when test="${sessionScope.sessionLocale=='ua'}">
                <a class="nav-link"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sessionLocale=en">
                    EN
                </a>
                <a class="nav-link active"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sessionLocale=ua">
                    UA
                </a>
            </c:when>
        </c:choose>
    </div>
    <c:choose>
        <c:when test="${sessionScope.role== null}">
            <div class="topnav-right">
                <li class="nav-item dropdown">
                    <a href="${pageContext.request.contextPath}/eLibrary?command=login"><fmt:message bundle="${loc}"
                                                                                                     key="menu.login"/></a>
                </li>
            </div>
        </c:when>
        <c:when test="${sessionScope.role != null}">
            <div class="topnav-right">
                    <a
                       href="${pageContext.request.contextPath}/eLibrary?command=cabinetPage"
                       role="button"
                       aria-haspopup="true" aria-expanded="false">
                            ${sessionScope.user.email}
                    </a>
                    <a href="${pageContext.request.contextPath}/eLibrary?command=logout"><fmt:message bundle="${loc}"
                                                                                                      key="menu.logout"/></a>
            </div>
        </c:when>

    </c:choose>
    <c:choose>
        <c:when test="${sessionScope.role.id == 2}">
            <div class="topnav-right">
                <a href="${pageContext.request.contextPath}/eLibrary?command=orders"><fmt:message bundle="${loc}"
                                                                                                  key="menu.orders"/></a>
                <a href="${pageContext.request.contextPath}/eLibrary?command=readers"><fmt:message bundle="${loc}"
                                                                                                   key="menu.users"/></a>
            </div>
        </c:when>
        <c:when test="${sessionScope.role.id == 3}">
            <div class="topnav-right">
                <a href="${pageContext.request.contextPath}/eLibrary?command=addBookPage"><fmt:message bundle="${loc}"
                                                                                                       key="menu.addBook"/></a>
                <a href="${pageContext.request.contextPath}/eLibrary?command=librariansPage"><fmt:message
                        bundle="${loc}" key="menu.librarians"/></a>
            </div>
        </c:when>
    </c:choose>

</div>
</body>
</html>

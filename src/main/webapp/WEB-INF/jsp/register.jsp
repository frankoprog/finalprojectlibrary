<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<fmt:setLocale value="${sessionScope.sessionLocale}"/>
<fmt:setBundle basename="langResources.langResources" var="loc"/>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login.css">
</head>
<jsp:include page="../../menu.jsp"/>
<body>
<div class=loginContainer>
    <div class=form>
        <h3><fmt:message bundle="${loc}" key="registration.label"/></h3>
        <form method="post" action="${pageContext.request.contextPath}/eLibrary?command=signUp">
            <h4><fmt:message bundle="${loc}" key="registration.name"/></h4><input type="text" name="name"/>
            <h4><fmt:message bundle="${loc}" key="registration.surname"/></h4><input type="text" name="surname"/>
            <h4><fmt:message bundle="${loc}" key="registration.email"/></h4><input type="text" name="email">
            <h4><fmt:message bundle="${loc}" key="registration.telephone"/></h4><input type="text" name="phone">
            <h4><fmt:message bundle="${loc}" key="registration.password"/></h4> <input type="password" name="password"/><br>
            <input type="submit" value=<fmt:message bundle="${loc}" key="registration.register"/>>
        </form>
    </div>
</div>
<%--<a href="${pageContext.request.contextPath}/eLibrary?command=home">Home</a>--%>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Orders</title>
    <link rel="stylesheet" href="../../css/table.css">
    <link rel="stylesheet" href="../../css/readerMainStyle.css">
    <link rel="stylesheet" href="../../css/bookCard.css">
</head>
<body>
<fmt:setLocale value="${sessionScope.sessionLocale}"/>
<fmt:setBundle basename="langResources.langResources" var="loc"/>
<jsp:include page="../../menu.jsp"/>
<div align="center">
    <table c>
        <caption><h2><fmt:message bundle = "${loc}" key ="orders.caption"/></h2></caption>
        <tr>
            <th><fmt:message bundle = "${loc}" key ="orders.orderId"/></th>
            <th><fmt:message bundle = "${loc}" key ="readersView.userId"/></th>
            <th><fmt:message bundle = "${loc}" key ="cabinet.idBook"/></th>
        </tr>
        <c:forEach var="order" items="${requestScope.orders}">
            <tr>
                <td><c:out value="${order.idOrder}"/></td>
                <td><c:out value="${order.idUser}"/></td>
                <td><c:out value="${order.idBook}"/></td>
                <td>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=approveOrder">
                        <button type="submit" value=${order.idOrder} name="orderId"><fmt:message bundle = "${loc}" key ="readersView.lend"/></button>
                    </form>
                </td>
                <td>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=deleteOrder">
                        <button type="submit" value=${order.idOrder} name="orderIdDelete"><fmt:message bundle = "${loc}" key ="readersView.cancel"/></button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>addBook</title>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
<div class=loginContainer>
    <div class=form>
        <form method="post" action="${pageContext.request.contextPath}/controller?command=addBook">
            BookName:
            <input type="text" name="bookName"><br/>
            AuthorName:
            <input type="text" name="authorName"><br/>
            Publisher:
            <input type="text" name="publisherName"><br/>
            <input type="submit" value=Add>
        </form>
    </div>
</div>
</body>
</html>

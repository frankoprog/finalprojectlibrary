<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="p" uri="/WEB-INF/jsp/tld/sort.tld" %>
<html>
<head>
    <title>Catalog</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
</head>
<body class=bg-body>
<jsp:include page="../../menu.jsp"/>
<fmt:setLocale value="${sessionScope.sessionLocale}"/>
<fmt:setBundle basename="langResources.langResources" var="loc"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css"/>

<div align="center">
    <caption><h2><fmt:message bundle="${loc}" key="catalog.caption"/></h2></caption>
    <c:choose>
        <c:when test="${sessionScope.sort == 'author'}">
            <li class="nav-item">
                <a class="nav-link active"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sort=author">
                    <fmt:message bundle="${loc}"
                                 key="sort.byAuthor"/>
                    <c:forEach items="${p:sortByAuthor(books)}" var="books"/>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sort=name">
                    <fmt:message bundle="${loc}"
                                 key="sort.byName"/>
                    <c:forEach items="${p:sortByAuthor(books)}" var="books"/>
                </a>
            </li>
        </c:when>
        <c:when test="${sessionScope.sort=='name'}">
            <li class="nav-item">
                <a class="nav-link"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sort=author">
                    <fmt:message bundle="${loc}"
                                 key="sort.byAuthor"/>
                    <c:forEach items="${p:sortByName(books)}" var="books"/>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active"
                   href="${pageContext.request.contextPath}/eLibrary?${pageContext.request.queryString}&sort=name">
                    <fmt:message bundle="${loc}"
                                 key="sort.byName"/>
                    <c:forEach items="${p:sortByName(books)}" var="books"/>
                </a>
            </li>
        </c:when>
    </c:choose>
    <table class=table align="center" cellpadding="5">
        <caption>
            <form method="post" action="${pageContext.request.contextPath}/controller?command=searchBooks">
                <label>
                    <input type="text" placeholder=
                    <fmt:message bundle="${loc}" key="catalog.search_placeholder"/> name="searchText">
                </label>
                <button type="submit" value="" name="bookId"><fmt:message bundle="${loc}" key="catalog.find"/></button>
            </form>
        </caption>
        <tr>
            <c:choose>
                <c:when test="${sessionScope.role.id == 3}">
                    <th><fmt:message bundle="${loc}" key="catalog.editBook"/></th>
                    <th><fmt:message bundle="${loc}" key="catalog.deleteBook"/></th>
                </c:when>
            </c:choose>
            <c:if test="${sessionScope.role.id == 3}">
                <th>ID</th>
            </c:if>
            <th><fmt:message bundle="${loc}" key="catalog.title"/></th>
            <th><fmt:message bundle="${loc}" key="catalog.author"/></th>
            <th><fmt:message bundle="${loc}" key="catalog.publisher"/></th>
        </tr>
        <c:choose>
            <c:when test="${requestScope.searchedBooks == null}">
                <c:forEach var="book" items="${requestScope.books}">
                    <c:choose>
                        <c:when test="${sessionScope.role.id == 3}">
                            <tr>
                            <form method="post"
                                  action="${pageContext.request.contextPath}/controller?command=editBookPage">
                                <td>
                                    <button type="submit" value=${book.id} name="bookId"><fmt:message bundle="${loc}"
                                                                                                      key="catalog.editBook"/></button>
                                </td>
                            </form>
                            <form method="post"
                                  action="${pageContext.request.contextPath}/controller?command=deleteBook">
                                <td>
                                    <button type="submit" value=${book.id} name="bookIdDelete"><fmt:message
                                            bundle="${loc}" key="catalog.deleteBook"/></button>
                                </td>
                            </form>
                        </c:when>
                    </c:choose>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=addOrder">
                        <c:if test="${sessionScope.role.id == 3}">
                            <td><c:out value="${book.id}"/></td>
                        </c:if>
                        <td><c:out value="${book.name}"/></td>
                        <td><c:out value="${book.author}"/></td>
                        <td><c:out value="${book.publisherName}"/></td>
                        <c:choose>
                            <c:when test="${sessionScope.user != null}">
                                <td>
                                    <button type="submit" value=${book.id} name="bookId"><fmt:message bundle="${loc}"
                                                                                                      key="catalog.order"/></button>
                                </td>
                            </c:when>
                        </c:choose>
                    </form>
                    </tr>
                </c:forEach>
            </c:when>
            <c:when test="${requestScope.searchedBooks != null}">
                <c:forEach var="book1" items="${requestScope.searchedBooks}">
                    <tr>
                    <c:choose>
                        <c:when test="${sessionScope.role.id == 3}">
                            <tr>
                            <form method="post"
                                  action="${pageContext.request.contextPath}/controller?command=editBookPage">
                                <td>
                                    <button type="submit" value=${book1.id} name="bookId"><fmt:message bundle="${loc}"
                                                                                                       key="catalog.editBook"/></button>
                                </td>
                            </form>
                            <form method="post"
                                  action="${pageContext.request.contextPath}/controller?command=deleteBook">
                                <td>
                                    <button type="submit" value=${book1.id} name="bookIdDelete"><fmt:message
                                            bundle="${loc}" key="catalog.editBook"/></button>
                                </td>
                            </form>
                        </c:when>
                    </c:choose>
                    <form method="post" action="${pageContext.request.contextPath}/controller?command=addOrder">
                        <c:if test="${sessionScope.role.id == 3}">
                            <td><c:out value="${book.id}"/></td>
                        </c:if>
                        <td><c:out value="${book1.name}"/></td>
                        <td><c:out value="${book1.author}"/></td>
                        <td><c:out value="${book1.publisherName}"/></td>
                        <c:choose>
                            <c:when test="${sessionScope.user != null}">
                                <td>
                                    <button type="submit" value=${book1.id} name="bookId">Order</button>
                                </td>
                            </c:when>
                        </c:choose>
                    </form>
                    </tr>
                </c:forEach>
            </c:when>
        </c:choose>
    </table>
    <a href="${pageContext.request.contextPath}/eLibrary?command=catalog&page=1"><fmt:message bundle="${loc}"
                                                                                              key="catalog.firstPage"/></a>
    <c:if test="${page != 1}">
        <td><a href="eLibrary?command=catalog&page=${page - 1}"><--</a></td>
    </c:if>
    <b>${requestScope.page}</b>
    <c:if test="${page lt nuPages}">
        <td><a href="eLibrary?command=catalog&page=${page + 1}">--></a></td>
    </c:if>
    <a href="${pageContext.request.contextPath}/eLibrary?command=catalog&page=${requestScope.nuPages}"><fmt:message
            bundle="${loc}" key="catalog.lastPage"/></a>
    <br>
</div>
</body>
</html>

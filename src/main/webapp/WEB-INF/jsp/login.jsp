<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<fmt:setLocale value="${sessionScope.sessionLocale}"/>
<fmt:setBundle basename="langResources.langResources" var="loc"/>
<html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
<jsp:include page="../../menu.jsp"/>
<div class=loginContainer>
    <div class=form>
        <h3><fmt:message bundle="${loc}" key="label.authorization"/></h3>
        <form method="post" action="${pageContext.request.contextPath}/controller?command=checkLogin">
            <input type="hidden" name="command" value="Login"/>
            <input type="text" placeholder=
            <fmt:message bundle="${loc}" key="label.email"/> name="email"
                   pattern="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
                   aria-describedby="emailHelp">
            <input type="password" placeholder=
            <fmt:message bundle="${loc}" key="label.password"/> name="password"/>
            <input type="submit" value=
            <fmt:message bundle="${loc}" key="label.login"/> id="btnLog">
            <div class=register>
                <a href="${pageContext.request.contextPath}/eLibrary?command=signUpPage"><span class="txt1">
						 <fmt:message bundle="${loc}" key="label.register"/>
						</span></a>
            </div>
        </form>
    </div>
</div>
<div class="footer">
</div>
</body>
</html>
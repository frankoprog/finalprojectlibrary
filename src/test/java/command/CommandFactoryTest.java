package command;

import com.epam.franko.finalprojectlibrary.command.Command;
import com.epam.franko.finalprojectlibrary.command.CommandFactory;
import com.epam.franko.finalprojectlibrary.command.CommandName;
import com.epam.franko.finalprojectlibrary.command.action.HomePageCommand;
import com.epam.franko.finalprojectlibrary.command.action.autorization.*;
import com.epam.franko.finalprojectlibrary.command.action.cabinet.CabinetPageCommand;
import com.epam.franko.finalprojectlibrary.command.action.catalog.*;
import com.epam.franko.finalprojectlibrary.command.action.librarians.LibrarinsPageCommand;
import com.epam.franko.finalprojectlibrary.command.action.orders.ApproveOrderCommand;
import com.epam.franko.finalprojectlibrary.command.action.orders.DeleteOrderCommand;
import com.epam.franko.finalprojectlibrary.command.action.orders.OrdersCommand;
import com.epam.franko.finalprojectlibrary.command.action.readers.ReadersCommand;
import com.epam.franko.finalprojectlibrary.command.action.readers.ReadersOrdersCommand;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandFactoryTest {
    @Test
    public void testCreateCommand_ShouldReturnAddToBasketCommand_WhenCommandIsAddToBasket() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.HOME_COMMAND);
        assertEquals(actual.getClass(), HomePageCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnCompleteOrderCommand_WhenCommandIsCompleteUserOrder() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.LOG_IN_COMMAND);
        assertEquals(actual.getClass(), LoginPageCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnConfirmAddingProductCommand_WhenCommandIsConfirmAddingProduct() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.SIGN_UP_PAGE_COMMAND);
        assertEquals(actual.getClass(), SingUpPageCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnConfirmOrderCommand_WhenCommandIsConfirmOrder() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.CHECK_LOGIN_COMMAND);
        assertEquals(actual.getClass(), LogInCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnConfirmProductChangeCommand_WhenCommandIsConfirmProductChange() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.LOG_OUT_COMMAND);
        assertEquals(actual.getClass(), LogOutCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnDeleteOrderCommand_WhenCommandIsDeleteOrder() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.CABINET_PAGE_COMMAND);
        assertEquals(actual.getClass(), CabinetPageCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnDeleteUserOrderCommand_WhenCommandIsDeleteUserOrder() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.SIGN_UP_COMMAND);
        assertEquals(actual.getClass(), SignUpCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnLogInCommand_WhenCommandIsCheckLogin() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.CATALOG_PAGE_COMMAND);
        assertEquals(actual.getClass(), CatalogPageCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnLogOutCommand_WhenCommandIsLogOut() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.ORDERS_COMMAND);
        assertEquals(actual.getClass(), OrdersCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToAddOrderCommand_WhenCommandIsAddOrder() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.READERS_COMMAND);
        assertEquals(actual.getClass(), ReadersCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToAddProductCommand_WhenCommandIsAddProduct() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.ADD_ORDER_COMMAND);
        assertEquals(actual.getClass(), AddOrderCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToBasketCommand_WhenCommandIsBasket() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.APPROVE_ORDER_COMMAND);
        assertEquals(actual.getClass(), ApproveOrderCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToCatalogCommand_WhenCommandIsCatalog() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.DELETE_ORDER_COMMAND);
        assertEquals(actual.getClass(), DeleteOrderCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToContactsCommand_WhenCommandIsContacts() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.READERS_ORDERS_COMMAND);
        assertEquals(actual.getClass(), ReadersOrdersCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToEditProductCommand_WhenCommandIsEditProduct() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.SEARCH_CATALOG_COMMAND);
        assertEquals(actual.getClass(), SearchBookCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToLogInCommand_WhenCommandIsLogIn() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.ADD_BOOK_PAGE_COMMAND);
        assertEquals(actual.getClass(), AddBookPageCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToLogUpCommand_WhenCommandIsLogUp() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.ADD_BOOK_COMMAND);
        assertEquals(actual.getClass(), AddBookCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToMainCommand_WhenCommandIsMain() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.DELETE_BOOK_COMMAND);
        assertEquals(actual.getClass(), DeleteBookCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToMyOrdersCommand_WhenCommandIsMyOrders() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.EDIT_BOOK_PAGE_COMMAND);
        assertEquals(actual.getClass(), EditBookPageCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToPromotionsCommand_WhenCommandIsPromotions() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.EDIT_BOOK_COMMAND);
        assertEquals(actual.getClass(), EditBookCommand.class);
    }

    @Test
    public void testCreateCommand_ShouldReturnGoToViewOrdersCommand_WhenCommandIsViewOrders() {
        Command actual = CommandFactory.getInstance().getCommand(CommandName.LIBRARIANS_PAGE_COMMAND);
        assertEquals(actual.getClass(), LibrarinsPageCommand.class);
    }
}

package service;

import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.UserService;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class UserServiceTest {
    private static UserService userService;

    @BeforeAll
    static void initializeConnectionPool() throws DaoException, ServiceException {
        userService = new UserServiceImpl();
    }

    @Test
    public void testLoginShouldReturnUser() throws ServiceException {
        Optional<User> actual = userService.findByEmailAndPassword("admin", "d033e22ae348aeb5660fc2140aec35850c4da997");
        assertTrue(actual.isPresent());
    }

    @Test
    public void testLoginShouldReturnNull() throws ServiceException {
        Optional<User> actual = userService.findByEmailAndPassword("Nothing", "Nothing");
        assertFalse(actual.isPresent());
    }

    @Test
    public void testLoginShouldReturnNull1() throws ServiceException {
        Optional<User> actual = userService.findByEmailAndPassword(null, null);
        assertFalse(actual.isPresent());
    }

    @Test
    public void testFindUserById() throws ServiceException {
        userService.register("testReg","surname","testreg@gmail.com","phone","1");
        Optional<User> actual = userService.findById(25);
        assertTrue(userService.findByEmailAndPassword("testreg@gmail.com", "1").isPresent());
    }

}


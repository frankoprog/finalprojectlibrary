package service;

import com.epam.franko.finalprojectlibrary.exceptions.DaoException;
import com.epam.franko.finalprojectlibrary.exceptions.ServiceException;
import com.epam.franko.finalprojectlibrary.interfaces.service.BookService;
import com.epam.franko.finalprojectlibrary.model.Book;
import com.epam.franko.finalprojectlibrary.model.User;
import com.epam.franko.finalprojectlibrary.service.BookServiceImpl;
import com.epam.franko.finalprojectlibrary.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BookServiceTest {
    private static BookService bookService;

    @BeforeAll
    static void initializeConnectionPool() throws DaoException, ServiceException {
        bookService = new BookServiceImpl();
    }

    @Test
    public void testFindByIdReturnNotNull() throws ServiceException {
        Optional<Book> actual = bookService.findById(1);
        assertTrue(actual.isPresent());
    }

    @Test
    public void testFindByIdReturnNull() throws ServiceException {
        Optional<Book> actual = bookService.findById(-1);
        assertFalse(actual.isPresent());
    }

    @Test
    public void testFindByNameReturnNotNull() throws ServiceException {
        List<Book> actual = bookService.findByName("Dobriy");
        assertTrue(actual.size()!=0);
    }
    @Test
    public void testSaveBookAndIsPresent() throws ServiceException {
       Book book = new Book();
       book.setName("test");
       book.setAuthor("test");
       book.setSynopsis("test");
       book.setPublisherName("test");
       bookService.save(book);
       List<Book> bookFind = bookService.findByName("test");
        assertTrue(bookFind.size()!=0);



    }
}
